﻿using Amazon.S3;
using Amazon.S3.Model;
using FileHelpers;
using FileHelpers.Events;
using ICSharpCode.SharpZipLib.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using log4net;
using NewAgileReport.Pojo;
using NewAgileReport.Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.UI;

namespace NewAgileReport.Utils
{
    class ReportUtils
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static string ITA = "ITA";
        private const int CSV_BUFFER_SIZE = 10000;
        private const int MAX_PDF_RECORDSET = 25000;
        private const int MAX_PDF_ROWS_PER_PAGE = 70;

        public static void CheckConfigParams()
        {
            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["BackupFilePath"]))
                throw new Exception("Param BackupFilePath is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPAlias"]))
                throw new Exception("Param SMTPAlias is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSProfileName"]))
                throw new Exception("Param AWSProfileName is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPSSL"]))
                throw new Exception("Param SMTPSSL is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPPassword"]))
                throw new Exception("Param SMTPPassword is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPUser"]))
                throw new Exception("Param SMTPUser is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPPort"]))
                throw new Exception("Param SMTPPort is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["SMTPHost"]))
                throw new Exception("Param SMTPHost is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSRegion"]))
                throw new Exception("Param AWSRegion is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSProfilesLocation"]))
                throw new Exception("Param AWSProfilesLocation is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["WorkersNumber"]))
                throw new Exception("Param WorkersNumber is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["PDFLogoFilePath"]))
                throw new Exception("Param PDFLogoFilePath is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["AWSLinkExpirationInMinutes"]))
                throw new Exception("Param AWSLinkExpirationInMinutes is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["PDFBackgroundFilePath"]))
                throw new Exception("Param PDFBackgroundFilePath is null or Empty");

            if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["ReportQueueLimit"]))
                throw new Exception("Param ReportQueueLimit is null or Empty");

        }

        public static String CreateCsv(DbDataReader reader, ReportRequestObject ro, Boolean hideDelivery, out SMSObject smsObject)
        {
            DelimitedFileEngine engine = new DelimitedFileEngine(typeof(ReportCsv));
            engine.HeaderText = ReportCsv.header;
            smsObject = new SMSObject();
            smsObject.CostSms = 0;
            smsObject.CountSms = 0;
            List<ReportCsv> reportCsvList = new List<ReportCsv>();

            String path = ConfigurationManager.AppSettings["BackupFilePath"];
            if (!path.EndsWith("\\"))
                path += "\\";
            String filename = path + ro.ReportId + "_" + Guid.NewGuid().ToString() + ".csv";
            FileInfo f = new FileInfo(filename);
            if (f.Exists)
                throw new Exception("Il file " + filename + " Esiste!!!!!!!!");
            engine.WriteFile(filename, reportCsvList);

            using (reader)
            {
                while (reader.Read())
                {
                    ArchiveObject ao = DbUtils.FillArchiveObject(reader);
                    ReportCsv report = new ReportCsv();
                    smsObject.CountSms++;
                    report.rowNumber = smsObject.CountSms;
                    smsObject.CostSms += ao.BaseCost;
                    string date = ao.DeliveredDateTime.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    string deliveryDateTime = "";

                    if (ao.DeliveryDateTime != DateTime.MinValue)
                        deliveryDateTime = ao.DeliveryDateTime.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    else
                        deliveryDateTime = "";
                    report.dateTime = date;
                    report.originator = ao.Sender;
                    report.destination = ao.Destination;
                    string txt = ao.Text.Trim();
                    txt = AdjustText(txt, false);
                    report.message = (txt.Replace(";", " "));
                    //report.message = txt;
                    String reqGw = "";
                    if (ao.RequestedGateway >= 0)
                        reqGw = ao.RequestedGateway.ToString();
                    String dlrv = "";
                    if (reqGw == "100")
                        dlrv = "n/a";
                    else
                        dlrv = getMyDLVR(hideDelivery, ao.DeliveryStatus.ToString(), deliveryDateTime);

                    report.deliveryStatus = dlrv;
                    report.ipAddress = ao.IpAddress;
                    report.subAccount = ao.IdUser;

                    reportCsvList.Add(report);

                    if (reportCsvList.Count >= CSV_BUFFER_SIZE)
                    {
                        engine.AppendToFile(filename, reportCsvList);
                        reportCsvList.Clear();
                    }
                }
            }
            if (reportCsvList.Count > 0)
            {
                engine.AppendToFile(filename, reportCsvList);
                reportCsvList.Clear();
            }

            if (smsObject.CountSms == 0)
                f.Delete();

            return filename;
        }

        public static String AmazonS3Upload(String bucketName, String fileName)
        {
            String key = Path.GetFileNameWithoutExtension(fileName) + Path.GetExtension(fileName);
            using (IAmazonS3 client = new AmazonS3Client(Amazon.RegionEndpoint.EUWest1))
            {
                PutObjectRequest request = new PutObjectRequest()
                {
                    BucketName = bucketName,
                    Key = key,
                    FilePath = fileName
                };
                PutObjectResponse response2 = client.PutObject(request);

                GetPreSignedUrlRequest request1 = new GetPreSignedUrlRequest()
                {
                    BucketName = bucketName,
                    Key = key,
                    Expires = DateTime.Now.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["AWSLinkExpirationInMinutes"]))
                };

                return client.GetPreSignedURL(request1);
            }
        }

        public static void AliveFaro(string pathFile, string fileName, string text)
        {
            try
            {

                // Create an instance of StreamWriter to write text to a file.
                // The using statement also closes the StreamWriter.
                if (pathFile.EndsWith("\\") == false)
                    pathFile = pathFile + "\\";
                using (StreamWriter sw = new StreamWriter(pathFile + fileName))
                {
                    // Add some text to the file.                
                    if (text == "")
                        sw.WriteLine(DateTime.Now);
                    else
                        sw.WriteLine(text);
                }
                    
            }
            catch(Exception exc)
            {
                logger.Fatal(exc.Message, exc);
            }
        }

        public static void SendEmail(ReportRequestObject ro, SMSObject so, String attchmentLink)
        {
            using (SmtpClient xsmtp = new SmtpClient())
            {
                String host = ConfigurationManager.AppSettings["SMTPHost"];
                String port = ConfigurationManager.AppSettings["SMTPPort"];
                String user = ConfigurationManager.AppSettings["SMTPUser"];
                String password = ConfigurationManager.AppSettings["SMTPPassword"];
                String ssl = ConfigurationManager.AppSettings["SMTPSSL"];
                String alias = ConfigurationManager.AppSettings["SMTPAlias"];

                NetworkCredential auth = new NetworkCredential(user, password);
                String dateFrom = ro.ReportStartDatetime.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                String dateTo = DatetimeOnReport(ro.ReportEndDatetime.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture));
                xsmtp.Host = host;
                xsmtp.Port = Convert.ToInt32(port);
                xsmtp.UseDefaultCredentials = false;
                xsmtp.Credentials = auth;
                xsmtp.EnableSsl = Convert.ToBoolean(ssl);
                //Imposto un timeout di 5 minuti
                //xsmtp.Timeout = 300 * 1000;

                using (MailMessage xemail = new MailMessage())
                {
                    xemail.Subject = EmailSubject(ro.PaeseIso3);
                    xemail.From = new MailAddress(alias, alias);
                    xemail.ReplyToList.Add(new MailAddress(alias, alias));
                    //xemail.To.Add(ro.ReportEmail);
                    xemail.To.Add("l.colucci@agiletelecom.com");
                    xemail.IsBodyHtml = true;
                    //xemail.Bcc.Add("l.colucci@agiletelecom.com");
                    xemail.Body = HtmlBody(so.CountSms, so.CostSms,dateFrom,dateTo, ro.ReportLoginParam,
                        ro.ReportSenderParam, ro.ReportDestinyParam, attchmentLink, ro.PaeseIso3);
                    Boolean spedita = false;
                    int contaErrori = 0;
                    do
                    {
                        try
                        {
                            xsmtp.Send(xemail);
                            spedita = true;
                        }
                        catch (Exception exc)
                        {
                            contaErrori = contaErrori + 1;
                            spedita = false;
                            if (contaErrori == 3)
                                throw exc;
                            Thread.Sleep(1000);
                        }
                    }
                    while (!spedita);
                }
            }           

        }

        private static string HtmlBody(long numSms, Double totalCost,
            string dateFrom, string dateTo, string user, string sender, 
            string destination, String attachmentLink, string stato)
        {

            string htmlEmail = "";
            String emptyMessage = "";
            String numSmsString = "";
            String costoSmsFormat = "";

            if (stato == ITA)
            {
                emptyMessage = Email.EmptyMessageITA;
                sender = Email.MittenteITA + ": " + sender;
                costoSmsFormat = Email.CostoITA + ": " + FormattaEuroDaMillesimi(totalCost, new CultureInfo("it-IT"));
                destination = Email.DestinatarioITA + ": " + destination;
                htmlEmail = Email.EmailITA.Replace("#USER#", user).Replace("#DATE_FROM#", dateFrom).Replace("#DATE_TO#", dateTo);
                numSmsString = Email.SMSInviatiITA + ": " + numSms.ToString("#,##0", CultureInfo.CreateSpecificCulture("it-IT"));
            }
            else
            {
                emptyMessage = Email.EmptyMessageForeign;
                sender = Email.MittenteForeign + ": " + sender;
                costoSmsFormat = Email.CostoForeign + ": " + FormattaEuroDaMillesimi(totalCost, new CultureInfo("en-GB"));
                destination = Email.DestinatarioForeign + ": " + destination;
                htmlEmail = Email.EmailForeign.Replace("#USER#", user).Replace("#DATE_FROM#", dateFrom).Replace("#DATE_TO#", dateTo);
                numSmsString = Email.SMSInviatiForeign + ": " + numSms.ToString("#,##0", CultureInfo.CreateSpecificCulture("en-GB"));
            }
            if (destination != "")
                htmlEmail = htmlEmail.Replace("#DESTINATION#", destination + "<br/><br/>");
            else
                htmlEmail = htmlEmail.Replace("#DESTINATION#", "");
            if (sender != "")
                htmlEmail = htmlEmail.Replace("#SENDER#", sender + "<br/><br/>");
            else
                htmlEmail = htmlEmail.Replace("#SENDER#", "");
            if (numSms == 0)
            {
                htmlEmail = htmlEmail.Replace("#TOTAL_COST#", "");
                htmlEmail = htmlEmail.Replace("#TOTAL#", "");
                htmlEmail = htmlEmail.Replace("#EMPTY#", emptyMessage + "<br/><br/>");
            }
            else
            {
                htmlEmail = htmlEmail.Replace("#TOTAL_COST#", costoSmsFormat + " EUR<br/><br/>");
                htmlEmail = htmlEmail.Replace("#TOTAL#", numSmsString);
                htmlEmail = htmlEmail.Replace("#EMPTY#", "");
            }
            if (numSms > 0 && !String.IsNullOrEmpty(attachmentLink))
                htmlEmail = htmlEmail.Replace("#ATTACH#", CreateReportLink(attachmentLink, "Download Report", true, "#000001"));
            else
                htmlEmail = htmlEmail.Replace("#ATTACH#", "");

            return htmlEmail;
        }

        private static String CreateReportLink(String url, String text, Boolean addBlankTarget, String hexColor)
        {
            if (String.IsNullOrEmpty(url))
                return String.Empty;

            StringBuilder stringBuilder = new StringBuilder();
            using (StringWriter stringWriter = new StringWriter(stringBuilder))
            {
                using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, url);
                    if (addBlankTarget)
                        writer.AddAttribute("target", "_blank");
                    writer.AddStyleAttribute(HtmlTextWriterStyle.Color, hexColor);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(text);
                    writer.RenderEndTag();
                }

            }

            return stringBuilder.ToString();

        }

        private static string DatetimeOnReport(string endDt)
        {
            //2008-12-31 23:59:59
            string orax = endDt.Substring(11).Trim();
            switch (orax)
            {
                case "00:59:59":
                    orax = "01:00:00";
                    break;
                case "01:59:59":
                    orax = "02:00:00";
                    break;
                case "02:59:59":
                    orax = "03:00:00";
                    break;
                case "03:59:59":
                    orax = "04:00:00";
                    break;
                case "04:59:59":
                    orax = "05:00:00";
                    break;
                case "05:59:59":
                    orax = "06:00:00";
                    break;
                case "06:59:59":
                    orax = "07:00:00";
                    break;
                case "07:59:59":
                    orax = "08:00:00";
                    break;
                case "08:59:59":
                    orax = "09:00:00";
                    break;
                case "09:59:59":
                    orax = "10:00:00";
                    break;
                case "10:59:59":
                    orax = "11:00:00";
                    break;
                case "11:59:59":
                    orax = "12:00:00";
                    break;
                case "12:59:59":
                    orax = "13:00:00";
                    break;
                case "13:59:59":
                    orax = "14:00:00";
                    break;
                case "14:59:59":
                    orax = "15:00:00";
                    break;
                case "15:59:59":
                    orax = "16:00:00";
                    break;
                case "16:59:59":
                    orax = "17:00:00";
                    break;
                case "17:59:59":
                    orax = "18:00:00";
                    break;
                case "18:59:59":
                    orax = "19:00:00";
                    break;
                case "19:59:59":
                    orax = "20:00:00";
                    break;
                case "20:59:59":
                    orax = "21:00:00";
                    break;
                case "21:59:59":
                    orax = "22:00:00";
                    break;
                case "22:59:59":
                    orax = "23:00:00";
                    break;
                case "23:59:59":
                    orax = "24:00:00";
                    break;
            }
            return endDt.Substring(0, 11) + " " + orax;
        }

        private static string EmailSubject(string STATO)
        {
            if (STATO == ITA)
                return Email.EmailSubjectITA;
            else
                return Email.EmailSubjectForeign;
        }

        private static String FormattaEuroDaMillesimi(Double val, CultureInfo cInfo)
        {
            return FormattaEuro(val / 1000, cInfo);
        }

        private static String FormattaEuro(Double val, CultureInfo cInfo)
        {
            val = Math.Round(val, 3, MidpointRounding.AwayFromZero);
            return val.ToString("##,###0.000", cInfo);
        }

        private static double ConvertBytesToMegabytes(long bytes)
        {
            return (bytes / 1024f) / 1024f;
        }

        public static String CreatePdfFromCsv(ReportRequestObject ro, String csvFile, int rowPerPage)
        {
            int paginaN = 1;
            DelimitedFileEngine engine = new DelimitedFileEngine(typeof(ReportCsv));
            //engine.BeforeReadRecord
            ReportCsv[] reportsfromCsv = (ReportCsv[])engine.ReadFile(csvFile);
            int totalPages = DammiTotPag(reportsfromCsv.Length, rowPerPage);
            Document document = new Document();
            iTextSharp.text.Rectangle myrect = new iTextSharp.text.Rectangle(PageSize.A4.Width, PageSize.A4.Height);
            document = new Document(myrect, 05, 05, 35, 0);
            String pdfFile = Path.ChangeExtension(csvFile, "pdf");
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(pdfFile, FileMode.Create));
            //writer.CloseStream = false;
            document.Open();
            iTextSharp.text.Image PDFimage = iTextSharp.text.Image.GetInstance(ConfigurationManager.AppSettings["PDFLogoFilePath"]);
            PDFimage.SetAbsolutePosition(10, 770);
            PDFimage.ScaleToFit(84, 53);
            iTextSharp.text.Image PDFimage2 = iTextSharp.text.Image.GetInstance(ConfigurationManager.AppSettings["PDFBackgroundFilePath"]);
            PDFimage2.SetAbsolutePosition(0, 0);
            PDFimage2.ScaleToFit(PageSize.A4.Width, PageSize.A4.Height);
            document.Add(PDFimage2);
            document.Add(PDFimage);
            
            string endDateTimeOnReport = DatetimeOnReport(ro.ReportEndDatetime.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture));
            string startDateTimeOnReport = ro.ReportStartDatetime.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
            string Sottotitolo = "";
            if (ro.PaeseIso3 == ITA)
                Sottotitolo = "User: " + ro.ReportLoginParam + " - Dal " + startDateTimeOnReport + " al " + endDateTimeOnReport + " - Pagina ";
            else
                Sottotitolo = "User: " + ro.ReportLoginParam + " - From date " + startDateTimeOnReport + " to date " + endDateTimeOnReport + " - Page ";
            Paragraph phrase3a = new Paragraph(". ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.BOLD, new BaseColor(255, 255, 255)));
            phrase3a.Alignment = 1;
            document.Add(phrase3a);
            Paragraph phrase3 = new Paragraph("Agile Telecom  SMS report", FontFactory.GetFont(FontFactory.HELVETICA, 14, Font.BOLD, new BaseColor(0, 0, 0)));
            phrase3.Alignment = 1;
            document.Add(phrase3);
            Paragraph phrase3b = new Paragraph(". ", FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.BOLD, new BaseColor(255, 255, 255)));
            phrase3b.Alignment = 1;
            document.Add(phrase3b);
            String of = "";
            if (ro.PaeseIso3 == ITA)
                of = " di ";
            else
                of = " of ";
            Paragraph phrase3c = new Paragraph((Sottotitolo + paginaN.ToString() + of + totalPages.ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL, new BaseColor(0, 0, 0)));
            phrase3c.Alignment = 1;
            document.Add(phrase3c);
            paginaN++;
            document.Add(phrase3b);
            String footer = "";
            if (ro.PaeseIso3 == ITA)
                footer = PDF.FooterITA;
            else
                footer = PDF.FooterForeign;
            Paragraph phrase5 = new Paragraph((footer), FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL, new BaseColor(150, 150, 150)));
            phrase5.Alignment = 1;
            phrase5.SpacingBefore = 30;

            PdfPTable aTable = new PdfPTable(7);   // 2 rows, 2 columns
            aTable.DefaultCell.Border = Rectangle.NO_BORDER;
            //aTable.BorderWidth = 0;
            //aTable.BorderColor = new iTextSharp.text.Color(99, 99, 99);
            //aTable.Spacing = 0;
            aTable.WidthPercentage = 85;
            String dateTime = "";
            String originator = "";
            String destination = "";
            String message = "";
            String deliveryStatus = "";
            String ipAddress = "";
            String subAccount = "";
            if(ro.PaeseIso3 == ITA)
            {
                dateTime = PDF.HeaderDatetimeITA;
                originator = PDF.HeaderOriginatorITA;
                destination = PDF.HeaderDestinationITA;
                message = PDF.HeaderMessageITA;
                deliveryStatus = PDF.HeaderDeliveryStatusITA;
                ipAddress = PDF.HeaderIPAddressITA;
                subAccount = PDF.HeaderSubAccountITA;
            }
            else
            {
                dateTime = PDF.HeaderDatetimeForeign;
                originator = PDF.HeaderOriginatorForeign;
                destination = PDF.HeaderDestinationForeign;
                message = PDF.HeaderMessageForeign;
                deliveryStatus = PDF.HeaderDeliveryStatusForeign;
                ipAddress = PDF.HeaderIPAddressForeign;
                subAccount = PDF.HeaderSubAccountForeign;
            }
            PdfPCell c1 = new PdfPCell(new Phrase(dateTime, FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD)));
            c1.HorizontalAlignment = 0;
            //c1.Width = 10;            
            aTable.AddCell(c1);
            PdfPCell c2 = new PdfPCell(new Phrase(originator, FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD)));
            c2.HorizontalAlignment = 0;
            //c2.Width = 8;
            aTable.AddCell(c2);
            PdfPCell c3 = new PdfPCell(new Phrase(destination, FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD)));
            c3.HorizontalAlignment = 0;
            //c3.Width = 10;
            aTable.AddCell(c3);
            PdfPCell c4 = new PdfPCell(new Phrase(message, FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD)));
            c4.HorizontalAlignment = 0;
            //c4.Width = 80;
            aTable.AddCell(c4);
            PdfPCell c5 = new PdfPCell(new Phrase(deliveryStatus, FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD)));
            c5.HorizontalAlignment = 0;
            //c5.Width = 8;
            aTable.AddCell(c5);
            PdfPCell c6 = new PdfPCell(new Phrase(ipAddress, FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD)));
            c6.HorizontalAlignment = 0;
            //c6.Width = 20;
            aTable.AddCell(c6);
            PdfPCell c7 = new PdfPCell(new Phrase(subAccount, FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.BOLD)));
            c7.HorizontalAlignment = 0;
            //c7.Width = 30;
            aTable.AddCell(c7);
            int maxP = rowPerPage;
            int cP = 0;

            foreach(ReportCsv reportCsv in reportsfromCsv)
            {
                cP++;
                PdfPCell cc1 = new PdfPCell(new Phrase(reportCsv.dateTime, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                cc1.HorizontalAlignment = 0;
                aTable.AddCell(cc1);
                PdfPCell cc2 = new PdfPCell(new Phrase(reportCsv.originator, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                cc2.HorizontalAlignment = 0;
                aTable.AddCell(cc2);
                PdfPCell cc3 = new PdfPCell(new Phrase(reportCsv.destination, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                cc3.HorizontalAlignment = 0;
                aTable.AddCell(cc3);
                String ttt = SistemaText(reportCsv.message, true);
                PdfPCell cc4 = new PdfPCell(new Phrase(ttt, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                cc4.HorizontalAlignment = 0;
                //cc4.Width = 80;
                aTable.AddCell(cc4);
                PdfPCell cc5 = new PdfPCell(new Phrase(reportCsv.deliveryStatus, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                cc5.HorizontalAlignment = 0;
                aTable.AddCell(cc5);
                PdfPCell cc6 = new PdfPCell(new Phrase(reportCsv.ipAddress, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                cc6.HorizontalAlignment = 0;
                aTable.AddCell(cc6);
                PdfPCell cc7 = new PdfPCell(new Phrase(reportCsv.subAccount, FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.NORMAL)));
                cc7.HorizontalAlignment = 0;
                aTable.AddCell(cc7);
                Paragraph phrase4 = new Paragraph((Sottotitolo + paginaN.ToString() + of + totalPages.ToString()), FontFactory.GetFont(FontFactory.HELVETICA, 8, Font.NORMAL, new BaseColor(0, 0, 0)));
                phrase4.Alignment = 1;

                if (cP >= maxP)
                {
                    document.Add(aTable);
                    document.Add(phrase5);
                    document.NewPage();
                    document.Add(PDFimage2);
                    document.Add(PDFimage);
                    document.Add(phrase3a);
                    document.Add(phrase3);
                    document.Add(phrase3b);
                    document.Add(phrase4);
                    document.Add(phrase3b);
                    aTable = new PdfPTable(7);
                    //aTable.DefaultCell.Border = Rectangle.NO_BORDER;
                    //aTable.BorderWidth = 0;
                    //aTable.BorderColor = new iTextSharp.text.Color(99, 99, 99);
                    //aTable.Padding = 0;
                    //aTable.Spacing = 0;
                    aTable.WidthPercentage = 85;
                    aTable.AddCell(c1);
                    aTable.AddCell(c2);
                    aTable.AddCell(c3);
                    aTable.AddCell(c4);
                    aTable.AddCell(c5);
                    aTable.AddCell(c6);
                    aTable.AddCell(c7);
                    paginaN++;
                    cP = 0;
                    maxP = rowPerPage;
                }
            }

            for (int iii = cP; iii < maxP; iii++)
            {
                PdfPCell ca1 = new PdfPCell(new Phrase(". ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.BOLD, BaseColor.WHITE)));
                ca1.HorizontalAlignment = 0;
                HideEndPageCellBorder(ca1);
                //ca1.Width = 10;
                aTable.AddCell(ca1);
                PdfPCell ca2 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.BOLD)));
                ca2.HorizontalAlignment = 0;
                HideEndPageCellBorder(ca2);
                //ca2.Width = 8;
                aTable.AddCell(ca2);
                PdfPCell ca3 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.BOLD)));
                ca3.HorizontalAlignment = 0;
                HideEndPageCellBorder(ca3);
                //ca3.Width = 10;
                aTable.AddCell(ca3);
                PdfPCell ca4 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.BOLD)));
                ca4.HorizontalAlignment = 0;
                HideEndPageCellBorder(ca4);
                //ca4.Width = 50;
                aTable.AddCell(ca4);
                PdfPCell ca5 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.BOLD)));
                ca5.HorizontalAlignment = 0;
                HideEndPageCellBorder(ca5);
                //ca5.Width = 8;
                aTable.AddCell(ca5);
                PdfPCell ca6 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.BOLD)));
                ca6.HorizontalAlignment = 0;
                HideEndPageCellBorder(ca6);
                //ca6.Width = 20;
                aTable.AddCell(ca6);
                PdfPCell ca7 = new PdfPCell(new Phrase(" ", FontFactory.GetFont(FontFactory.HELVETICA, 6, Font.BOLD)));
                ca7.HorizontalAlignment = 0;
                HideEndPageCellBorder(ca7);
                //ca7.Width = 30;
                aTable.AddCell(ca7);
            }
            document.Add(aTable);
            document.Add(phrase5);
            document.Close();

            return pdfFile;
        }

        private static void HideEndPageCellBorder(PdfPCell ca)
        {
            ca.BorderWidthTop = 0;
            ca.BorderWidthLeft = 0;
            ca.BorderWidthRight = 0;
            ca.BorderWidthBottom = 0;
        }

        private static int DammiTotPag(int m_RECORD_COUNT, int RowXpage)
        {
            if (m_RECORD_COUNT < 1)
                return 0;
            int x = m_RECORD_COUNT;
            int r = 0;
            while (x > 0)
            {
                r++;
                x = x - RowXpage;
            }
            return r;
        }

        private static string SistemaText(string tttx, bool taglia)
        {
            if (taglia)
            {
                if (tttx.Length > 18)
                    tttx = tttx.Substring(0, 15) + "...";
                else
                {
                    int xli = tttx.Length;
                    if (xli > 18)
                        xli = 18;
                    else
                        xli = tttx.Length;
                    tttx = tttx.Substring(0, xli);
                }
            }
            tttx = tttx.Replace("\n", " ");
            tttx = tttx.Replace("\r", " ");
            tttx = tttx.Replace("\b", " ");
            return tttx;
        }

        public static String CreateCsv(List<ArchiveObject> aoList, Boolean hideDelivery)
        {
            DelimitedFileEngine engine = new DelimitedFileEngine(typeof(ReportCsv));
            engine.HeaderText = ReportCsv.header;
            int csvRowCount = 0;
            List<ReportCsv> reportCsvList = new List<ReportCsv>();

            String filename = "C:\\Users\\luca.colucci\\Desktop\\reports\\" + Guid.NewGuid().ToString() + ".csv";
            FileInfo f = new FileInfo(filename);
            if (f.Exists)
                throw new Exception("Il file " + filename + " Esiste!!!!!!!!");
            engine.WriteFile(filename, reportCsvList);
            foreach (ArchiveObject ao in aoList)
            {
                ReportCsv report = new ReportCsv();
                csvRowCount++;
                report.rowNumber = csvRowCount;
                string date = ao.DeliveredDateTime.ToString("dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture);
                string deliveryDateTime = "";

                if (ao.DeliveryDateTime != DateTime.MinValue)
                    deliveryDateTime = ao.DeliveryDateTime.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                else
                    deliveryDateTime = "";
                report.dateTime = date;
                report.originator = ao.Sender;
                report.destination = ao.Destination;
                string txt = ao.Text.Trim();
                txt = AdjustText(txt, false);
                report.message = (txt.Replace(";", " "));
                String reqGw = "";
                if (ao.RequestedGateway >= 0)
                    reqGw = ao.RequestedGateway.ToString();
                String dlrv = "";
                if (reqGw == "100")
                    dlrv = "n/a";
                else
                    dlrv = getMyDLVR(hideDelivery, ao.DeliveryStatus.ToString(), deliveryDateTime);

                report.deliveryStatus = dlrv;
                report.ipAddress = ao.IpAddress;
                report.subAccount = ao.IdSubaccount;

                reportCsvList.Add(report);

                if (reportCsvList.Count >= 1000)
                {
                    engine.AppendToFile(filename, reportCsvList);
                    reportCsvList.Clear();
                }
            }
            if (reportCsvList.Count > 0)
            {
                engine.AppendToFile(filename, reportCsvList);
                reportCsvList.Clear();
            }

            return filename;
        }

        public static String CreateZipFile(String fileName)
        {
            FastZip zipFile = new FastZip();
            zipFile.CreateEmptyDirectories = true;
            String pdf = Path.GetDirectoryName(fileName) + "\\" + Path.GetFileNameWithoutExtension(fileName) + ".pdf";
            String csv = Path.GetDirectoryName(fileName) + "\\" + Path.GetFileNameWithoutExtension(fileName) + ".csv";
            FileInfo filePDF = new FileInfo(pdf);
            FileInfo fileCSV = new FileInfo(csv);
            String fileZip = "";
            if (fileCSV.Exists)
            {
                fileZip = Path.GetDirectoryName(fileName) + "\\" + Path.GetFileNameWithoutExtension(fileName) + ".zip";
                zipFile.CreateZip(fileZip, Path.GetDirectoryName(fileName), false, Path.GetFileNameWithoutExtension(fileName) + "\\.(pdf|csv)$", "");
                fileCSV.Delete();
                if (filePDF.Exists)
                    filePDF.Delete();  
            }

            return fileZip;

        }

        private static string AdjustText(string tttx, bool taglia)
        {
            if (taglia)
            {
                if (tttx.Length > 18)
                    tttx = tttx.Substring(0, 15) + "...";
                else
                {
                    int xli = tttx.Length;
                    if (xli > 18)
                        xli = 18;
                    else
                        xli = tttx.Length;
                    tttx = tttx.Substring(0, xli);
                }
            }
            tttx = tttx.Replace("\n", " ");
            tttx = tttx.Replace("\r", " ");
            tttx = tttx.Replace("\b", " ");
            return tttx;
        }

        private static string getMyDLVR(Boolean hideDelivery, string dlrStatus, string dateTimeString)
        {
            string dlrDescription = "Not requested";
            if (hideDelivery)
                dlrDescription = "n/a"; 
            else
            {
                switch (dlrStatus.Trim())
                {
                    case "0":
                        dlrDescription = "Unknown";
                        break;
                    case "1":
                        dlrDescription = "Accepted";
                        break;
                    case "2":
                        dlrDescription = "Rejected";
                        break;
                    case "3":
                        dlrDescription = dateTimeString;
                        break;
                    case "4":
                        dlrDescription = "Expired";
                        break;
                    case "6":
                        dlrDescription = "Undeliverable";
                        break;
                    case "7":
                        dlrDescription = "n/a";
                        break;
                    default:
                        dlrDescription = "Unknown";
                        break;
                }
            }
            return dlrDescription;
        }
    }

    [DelimitedRecord(";")]
    [IgnoreFirst(1)]
    class ReportCsv
    {
        public static readonly String header = "Row number;DateTime;Originator;Destination;Message;Delivery status;IP address;ID SubAccount";
        public Int32 rowNumber;
        public String dateTime;
        public String originator;
        public String destination;
        //[FieldQuoted('"', QuoteMode.AlwaysQuoted)]
        public String message;
        public String deliveryStatus;
        public String ipAddress;
        public String subAccount;
    }
}
