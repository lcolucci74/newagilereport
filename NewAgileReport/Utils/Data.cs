﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewAgileReport.Utils
{
    class Data
    {
        public class dbFreesmsMiddlePg
        {
            private NpgsqlConnection objConnection;

            public NpgsqlCommand GetFreesmsCommandObject(String strCommand)
            {
                NpgsqlConnection objConnection = GetFreesmsConnectionObject();
                if (objConnection.State != ConnectionState.Open)
                    objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                return objCommand;
            }
            public NpgsqlCommand GetFreesmsCommandObject(String strCommand, NpgsqlParameter[] parametersList)
            {
                NpgsqlConnection objConnection = GetFreesmsConnectionObject();
                if (objConnection.State != ConnectionState.Open)
                    objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                objCommand.Parameters.AddRange(parametersList);
                return objCommand;
            }
            public NpgsqlCommand GetFreesmsCommandObject(String strCommand, NpgsqlParameter param)
            {
                NpgsqlConnection objConnection = GetFreesmsConnectionObject();
                if (objConnection.State != ConnectionState.Open)
                    objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                objCommand.Parameters.Add(param);
                return objCommand;
            }

            public NpgsqlCommand GetFreesmsExecuteObject(String strCommand)
            {
                NpgsqlConnection objConnection = GetFreesmsConnectionObject();
                if (objConnection.State != ConnectionState.Open)
                    objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                objCommand.ExecuteNonQuery();
                return objCommand;
            }

            public NpgsqlCommand GetFreesmsExecuteObject(String strCommand, NpgsqlParameter[] parametersList)
            {
                NpgsqlConnection objConnection = GetFreesmsConnectionObject();
                if (objConnection.State != ConnectionState.Open)
                    objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                objCommand.Parameters.AddRange(parametersList);
                objCommand.ExecuteNonQuery();
                return objCommand;
            }
            public NpgsqlCommand GetFreesmsExecuteObject(String strCommand, NpgsqlParameterCollection parametersList)
            {
                NpgsqlConnection objConnection = GetFreesmsConnectionObject();
                if (objConnection.State != ConnectionState.Open)
                    objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                objCommand.Parameters.Add(parametersList);
                objCommand.ExecuteNonQuery();
                return objCommand;
            }
            public NpgsqlCommand GetFreesmsExecuteObject(String strCommand, NpgsqlParameter param)
            {
                NpgsqlConnection objConnection = GetFreesmsConnectionObject();
                if (objConnection.State != ConnectionState.Open)
                    objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                objCommand.Parameters.Add(param);
                objCommand.ExecuteNonQuery();
                return objCommand;
            }

            public NpgsqlConnection GetFreesmsConnectionObject()
            {
                if (objConnection == null || (objConnection.State == ConnectionState.Closed))
                {
                    String connection = ConfigurationManager.ConnectionStrings["report"].ToString();
                    objConnection = new NpgsqlConnection(connection);
                }
                return objConnection;
                //objConnection.Close;
            }
            public void CloseFreesmsConnectionObject(NpgsqlConnection objConnection)
            {
                if (objConnection != null || (objConnection.State != ConnectionState.Closed))
                {
                    objConnection.Close();
                    //objConnectionn.Dispose();
                }

            }
            public NpgsqlDataReader GetFreesmsReaderObject(String strCommand)
            {
                NpgsqlCommand objCommand = GetFreesmsCommandObject(strCommand);
                NpgsqlDataReader objReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return objReader;
            }
            public NpgsqlDataReader GetFreesmsReaderObject(String strCommand, NpgsqlParameter[] parametersList)
            {
                NpgsqlCommand objCommand = GetFreesmsCommandObject(strCommand);
                objCommand.Parameters.AddRange(parametersList);
                NpgsqlDataReader objReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return objReader;
            }
            public NpgsqlDataReader GetFreesmsReaderObject(String strCommand, NpgsqlParameter param)
            {
                NpgsqlCommand objCommand = GetFreesmsCommandObject(strCommand);
                objCommand.Parameters.Add(param);
                NpgsqlDataReader objReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return objReader;
            }

        }

        public class dbSmsdriverArchivio
        {
            private NpgsqlConnection objConnectionPostgreSQL;

            public NpgsqlConnection GetSmsdriverArchivioConnectionObjectPg()
            {
                if (objConnectionPostgreSQL == null)
                    objConnectionPostgreSQL = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["archivio"].ToString());
                return objConnectionPostgreSQL;
            }

            public NpgsqlCommand GetSmsdriverArchivioCommandObjectPg(String strCommand)
            {
                NpgsqlConnection objConnection = GetSmsdriverArchivioConnectionObjectPg();
                objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                return objCommand;
            }

            public NpgsqlCommand GetSmsdriverArchivioCommandObjectPg(String strCommand, NpgsqlParameter[] parametersList)
            {
                NpgsqlConnection objConnection = GetSmsdriverArchivioConnectionObjectPg();
                objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                objCommand.Parameters.AddRange(parametersList);
                return objCommand;
            }

            public NpgsqlCommand GetSmsdriverArchivioCommandObjectPg(String strCommand, NpgsqlParameter param)
            {
                NpgsqlConnection objConnection = GetSmsdriverArchivioConnectionObjectPg();
                objConnection.Open();
                NpgsqlCommand objCommand = new NpgsqlCommand(strCommand, objConnection);
                objCommand.Parameters.Add(param);
                return objCommand;
            }

            public NpgsqlDataReader GetSmsdriverArchivioReaderObjectPg(String strCommand)
            {
                NpgsqlCommand objCommand = GetSmsdriverArchivioCommandObjectPg(strCommand);
                objCommand.CommandTimeout = 60000;
                NpgsqlDataReader objReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return objReader;
            }

            public NpgsqlDataReader GetSmsdriverArchivioReaderObjectPg(String strCommand, NpgsqlParameter[] parametersList)
            {
                NpgsqlCommand objCommand = GetSmsdriverArchivioCommandObjectPg(strCommand);
                objCommand.CommandTimeout = 60000;
                objCommand.Parameters.AddRange(parametersList);
                NpgsqlDataReader objReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return objReader;
            }

            public NpgsqlDataReader GetSmsdriverArchivioReaderObjectPg(String strCommand, NpgsqlParameter param)
            {
                NpgsqlCommand objCommand = GetSmsdriverArchivioCommandObjectPg(strCommand);
                objCommand.CommandTimeout = 60000;
                objCommand.Parameters.Add(param);
                NpgsqlDataReader objReader = objCommand.ExecuteReader(CommandBehavior.CloseConnection);
                return objReader;
            }

            public DataSet GetSmsArchivioDataSetObjectPg(String strCommand)
            {
                DataSet ds = new DataSet("SmsArchivio");
                NpgsqlDataAdapter dbda = new NpgsqlDataAdapter(strCommand, GetSmsdriverArchivioConnectionObjectPg());
                dbda.Fill(ds);
                return ds;
            }
        }
    }
}
