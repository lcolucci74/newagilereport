﻿using log4net;
using NewAgileReport.Pojo;
using NewAgileReport.Worker;
using Quartz;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using static NewAgileReport.ExtendedBackgroundWorker;

namespace NewAgileReport.Utils
{
    public class ReportSenderJob : IInterruptableJob
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly static object lockObject = new object();
        private static List<ExtendedBackgroundWorker> workesList = null;
        private static ConcurrentQueue<ReportRequestObject> reportsQueue = new ConcurrentQueue<ReportRequestObject>();

        public delegate void QueueChangedHandler(int queueSize);
        public static event QueueChangedHandler baseQueueChanged;
        public delegate void ProduceReportHandler(JobMessages jobMessages);
        public static event ProduceReportHandler baseProduceReport;
        public delegate void NotifyHandler(ActionDebug action, String reportName, String message);
        public static event NotifyHandler baseNotify;
        public const int MAX_RECORDSET = 25000;

        public static void queueChanged(int queueSize)
        {
            baseQueueChanged?.Invoke(queueSize);
        }

        public static void produceReport(JobMessages jobMessages)
        {
            baseProduceReport?.Invoke(jobMessages);
        }

        public static void myNotifyToForm(ActionDebug action, String reportName, String message)
        {
            if (action == ActionDebug.LogMessage)
                message = DateTime.Now + ": " + message;
            baseNotify?.Invoke(action, reportName, message);
        }

        public static void CreateWorkersList(int size)
        {
            workesList = new List<ExtendedBackgroundWorker>();
            for (int i = 0; i < size; i++)
            {
                String workerName = "Worker_" + (i + 1);
                ExtendedBackgroundWorker bw = new ExtendedBackgroundWorker(workerName, true, 600000);
                bw.WorkerSupportsCancellation = true;
                bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundReportMaker_RunWorkerCompleted);
                bw.DoWork += new DoWorkEventHandler(backgroundReportMaker_DoWork);
                bw.WorkerTimeOutEvent += new WorkerTimeOutEventHandler(backgroundReportMaker_WorkertTimeout);
                workesList.Add(bw);
            }

        }

        private static void backgroundReportMaker_WorkertTimeout(Object sender)
        {
            ExtendedBackgroundWorker bw = (ExtendedBackgroundWorker)sender;
            myNotifyToForm(ActionDebug.LogMessage, bw.WorkerName, "TIMEOUT : " + bw.WorkerName + " DURATION : " + bw.WorkingDuration.TotalSeconds);
        }

        private static void backgroundReportMaker_DoWork(object sender, DoWorkEventArgs e)
        {
            ExtendedBackgroundWorker bw = (ExtendedBackgroundWorker)sender;
            ReportRequestObject ro = (ReportRequestObject)e.Argument;
            DateTime startReport = DateTime.Now;
            DateTime endReport = DateTime.Now;
            try
            {

                if (bw.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }             
                logger.Info("START REPORT " + ro.ReportId);
                myNotifyToForm(ActionDebug.UserName, bw.WorkerName, ro.ReportLoginParam);
                myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "QUERY DB " + ro.ReportId);
                myNotifyToForm(ActionDebug.DatagridStatus, bw.WorkerName, ((int)ReportStatus.Working).ToString());
                DbDataReader reader = DbUtils.reportQueryReader(ro, bw.CancellationPending);
                myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "CREAZIONE CSV " + ro.ReportId);
                //aoList.Clear()
                SMSObject smsObject = null;
                String csvFile = ReportUtils.CreateCsv(reader, ro, false, out smsObject);
                String attchmentLink = String.Empty;
                if (smsObject.CountSms > 0 && smsObject.CountSms <= MAX_RECORDSET)
                {
                    myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "CREAZIONE PDF " + ro.ReportId);
                    ReportUtils.CreatePdfFromCsv(ro, csvFile, 65);
                }
                if(smsObject.CountSms > 0)
                {
                    String zipFIle = ReportUtils.CreateZipFile(csvFile);
                    myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "UPLOAD TO AWS S3 " + ro.ReportId);
                    attchmentLink = ReportUtils.AmazonS3Upload("agile-report", zipFIle);
                }
                ReportUtils.SendEmail(ro, smsObject, attchmentLink);
                List<long> reportLsit = new List<long>();
                reportLsit.Add(ro.ReportId);
                endReport = DateTime.Now;
                TimeSpan duaration = endReport - startReport;
                DbUtils.UpdateReport(reportLsit, ReportStatusDb.ElaboratoOK, (int)duaration.TotalSeconds);

                myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "CREATED");
                if (bw.CancellationPending)
                    e.Cancel = true;
                else
                    e.Result = ro;

            }
            catch (Exception ex)
            {
                List<long> reportLsit = new List<long>();
                reportLsit.Add(ro.ReportId);
                endReport = DateTime.Now;
                TimeSpan duaration = endReport - startReport;
                DbUtils.UpdateReport(reportLsit, ReportStatusDb.ElaboratoKO, (int)duaration.TotalSeconds);
                e.Result = ex;
                logger.Fatal("Report ID : " + ro?.ReportId + ": " + ex.Message, ex);
                //if (ex is ThreadAbortException)
                //{
                //    myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "KILLED BY USER");
                //    myNotifyToForm(ActionDebug.DatagridStatus, bw.WorkerName, ((int)ReportStatus.Cancelled).ToString());
                //}

            }

        }

        private static void backgroundReportMaker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ExtendedBackgroundWorker bw = (ExtendedBackgroundWorker)sender;

            myNotifyToForm(ActionDebug.Duration, bw.WorkerName, "" + (int)bw.WorkingDuration.TotalSeconds);
            if (e.Error != null && !(e.Error is WorkerAbortException))
            {
                myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "GENERAL ERROR");
                myNotifyToForm(ActionDebug.DatagridStatus, bw.WorkerName, ((int)ReportStatus.GeneralError).ToString());
            }
            else if (e.Cancelled)
            {
                myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "CANCELLED BY USER");
                myNotifyToForm(ActionDebug.DatagridStatus, bw.WorkerName, ((int)ReportStatus.Cancelled).ToString());
            }
            else if (e.Result is Exception)
            {
                Exception error = (Exception)e.Result;
                myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "Error : " + error.Message);
                myNotifyToForm(ActionDebug.DatagridStatus, bw.WorkerName, ((int)ReportStatus.DoWorkError).ToString());
                myNotifyToForm(ActionDebug.Error, bw.WorkerName, "");
                consumeReportQueue(bw.WorkerName);
            }
            else
            {
                ReportRequestObject ro = (ReportRequestObject)e.Result;
                myNotifyToForm(ActionDebug.DatagridStatus, bw.WorkerName, ((int)ReportStatus.Completed).ToString());
                //myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "ID : " + ro.ReportId + " COMPLETED");
                consumeReportQueue(false);
            }
        }

        public static void flushQueue()
        {
            consumeReportQueue(true);
        }

        public static String getWorkerName(int index)
        {
            return workesList[index].WorkerName;
        }

        public static int getWorkerReportIndex(String reportName)
        {
            return workesList.FindIndex(x => x.WorkerName == reportName);
        }

        public static void KillAll()
        {
            lock(lockObject)
            {
                for (int i = 0; i < workesList.Count; i++)
                {
                    ExtendedBackgroundWorker bw = workesList[i];
                    if (bw.IsBusy)
                    {
                        myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "Killing......");
                        myNotifyToForm(ActionDebug.DatagridStatus, bw.WorkerName, ((int)ReportStatus.Killing).ToString());
                        workesList[i].Abort();
                    }
                }
            }
            
        }

        public static void StopAll()
        {
            lock (lockObject)
            {
                for (int i = 0; i < workesList.Count; i++)
                {
                    ExtendedBackgroundWorker bw = workesList[i];
                    if (bw.IsBusy)
                    {
                        myNotifyToForm(ActionDebug.DatagridInfo, bw.WorkerName, "Cancelling......");
                        myNotifyToForm(ActionDebug.DatagridStatus, bw.WorkerName, ((int)ReportStatus.Cancelling).ToString());
                        workesList[i].CancelAsync();
                    }
                }
            }
               
        }

        //static ReportSenderJob()
        //{
        //    reportsQueue.ContentChanged += OnObservableConcurrentQueueContentChanged;
        //}

        //private static void OnObservableConcurrentQueueContentChanged(
        //    object sender,
        //    NotifyConcurrentQueueChangedEventArgs<ReportObject> args)
        //{
        //    if (args.Action == NotifyConcurrentQueueChangedAction.Enqueue
        //        || args.Action == NotifyConcurrentQueueChangedAction.Peek)
        //    {
        //        consumeObservableReportQueue(false);

        //    }

        //}

        //public static void keepAliveQueue()
        //{
        //    if (reportsQueue.IsEmpty)
        //        return;
        //    ReportObject r = null;
        //    reportsQueue.TryPeek(out r);
        //}

        private static void consumeReportQueue(String workerName)
        {
            lock (lockObject)
            {
                int index = getWorkerReportIndex(workerName);
                if (index >= 0 && !workesList[index].IsBusy)
                {
                    ReportRequestObject ro = null;
                    ExtendedBackgroundWorker bw = workesList[index];
                    if (reportsQueue.TryDequeue(out ro))
                    {
                        bw.RunWorkerAsync(ro);
                        queueChanged(reportsQueue.Count);
                    }
                }
            }               
        }

        private static void consumeReportQueue(Boolean isRecursiveConsumer)
        {
            if (reportsQueue.IsEmpty)
                return;

            lock (lockObject)
            {
                int index = workesList.FindIndex(x => !x.IsBusy);
                if (index >= 0)
                {
                    ReportRequestObject ro = null;
                    ExtendedBackgroundWorker bw = workesList[index];
                    if (reportsQueue.TryDequeue(out ro))
                    {
                        bw.RunWorkerAsync(ro);
                        queueChanged(reportsQueue.Count);
                        if (isRecursiveConsumer)
                            consumeReportQueue(isRecursiveConsumer);
                    }

                }
            }
            
        }

        public void Execute(IJobExecutionContext context)
        {
            JobMessages jobMessage = new JobMessages();
            try
            {
                //Dictionary<String, ConcurrentQueue<ReportRequestObject>> dict = new Dictionary<string, ConcurrentQueue<ReportRequestObject>>();
                //List<ConcurrentQueue<ReportRequestObject>> queueList = dict.Where(key => key.Value.Count > 0).Select(k => k.Value).ToList();
                //jobMessage.ReportList = DbUtils.getReports(10);

                //jobMessage.ReportList = DbUtils.produceFakeReports(ref reportsQueue);
                jobMessage.ReportList = DbUtils.produceReports(ref reportsQueue);
                List<long> idReportList = jobMessage.ReportList.Select(x => x.ReportId).ToList();
                DbUtils.UpdateReport(idReportList, ReportStatusDb.InElaborazione, 0);

                queueChanged(reportsQueue.Count);
                consumeReportQueue(true);
                queueChanged(reportsQueue.Count);

                ReportUtils.AliveFaro("c:\\monitor", "ReportSender.txt", "");

            }
            catch (Exception exc)
            {
                jobMessage.Errors = exc;
            }

            produceReport(jobMessage);
        }

        public void Interrupt()
        {
            //TODO
        }
    }

    public class JobMessages
    {
        private List<ReportRequestObject> m_ReportList;
        private Exception m_Errors;

        public List<ReportRequestObject> ReportList
        {
            get
            {
                return m_ReportList;
            }

            set
            {
                m_ReportList = value;
            }
        }

        public Exception Errors
        {
            get
            {
                return m_Errors;
            }

            set
            {
                m_Errors = value;
            }
        }
    }
}
