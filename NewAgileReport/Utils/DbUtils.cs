﻿using log4net;
using NewAgileReport.Pojo;
using Npgsql;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static NewAgileReport.Utils.Data;

namespace NewAgileReport.Utils
{
    class DbUtils
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public static List<ReportRequestObject> produceReports(ref ConcurrentQueue<ReportRequestObject> reportsQueue)
        {
            int limit = Convert.ToInt16(ConfigurationManager.AppSettings["ReportQueueLimit"]);
            return produceReports(10, ref reportsQueue);
        }

        public static List<ReportRequestObject> produceReports(int limit, ref ConcurrentQueue<ReportRequestObject> reportsQueue)
        {
            List<ReportRequestObject> reportList = new List<ReportRequestObject>();
            dbFreesmsMiddlePg myDC = new dbFreesmsMiddlePg();

            String sql = "SELECT smsreport.*, smsaccount.smssubaccount as smssubaccount, ";
            sql += "CASE WHEN smsaccount.smssubaccount='N' THEN smsaccount.Paese ELSE (select Paese from smsaccount as sa WHERE sa.nc = smsaccount.ncupper) END as paese ";
            sql += "FROM smsreport, smsaccount ";
            sql += "WHERE lower(smsreport.report_loginparam) = lower(smsaccount.login) AND report_flagstate = 'N' ";
            sql += "ORDER BY report_requestdatetime desc LIMIT " + limit + ";";

            using (DbDataReader myR = myDC.GetFreesmsReaderObject(sql))
            {
                while (myR.Read())
                {
                    ReportRequestObject ro = FillReportRequestObject(myR);
                    reportList.Add(ro);
                    reportsQueue.Enqueue(ro);
                }
                
            }

            return reportList;
        }

        private static ReportRequestObject FillReportRequestObject(DbDataReader myR)
        {
            ReportRequestObject ro = new ReportRequestObject();
            ro.ReportId = Convert.ToInt32(myR["report_id"].ToString());
            ro.ReportRequestdatetime = myR.GetDateTime(myR.GetOrdinal("report_requestdatetime"));
            ro.ReportLoginParam = myR["report_loginparam"].ToString().Replace("##s##", "").ToLower();
            String subAccountString = myR["smssubaccount"].ToString();
            ro.IsSubAccount = subAccountString == "S" ? true : false;
            ro.PaeseIso3 = myR["paese"].ToString();
            ro.ReportStartDatetime = myR.GetDateTime(myR.GetOrdinal("report_startdatetime"));
            ro.ReportEndDatetime = myR.GetDateTime(myR.GetOrdinal("report_enddatetime"));
            ro.ReportSenderParam = myR["report_senderparam"].ToString();
            ro.ReportDestinyParam = myR["report_destinyparam"].ToString();
            ro.ReportEmail = myR["report_email"].ToString();
            ro.ReportFlagstate = myR["report_flagstate"].ToString();

            return ro;
        }

        public static List<ReportRequestObject> produceFakeReports(ref ConcurrentQueue<ReportRequestObject> reportsQueue)
        {
            dbFreesmsMiddlePg myDC = new dbFreesmsMiddlePg();

            List<ReportRequestObject> reportList = new List<ReportRequestObject>();

            int limit = new Random(Guid.NewGuid().GetHashCode()).Next(0, 2);

            String sql = "SELECT smsreport.*, smsaccount.smssubaccount as smssubaccount, ";
            sql += "CASE WHEN smsaccount.smssubaccount='N' THEN smsaccount.Paese ELSE (select Paese from smsaccount as sa WHERE sa.nc = smsaccount.ncupper) END as paese ";
            sql += "FROM smsreport, smsaccount ";
            sql += "WHERE lower(smsreport.report_loginparam) = lower(smsaccount.login) AND report_flagstate = 'S' ";
            sql += "AND report_requestdatetime >= @datemin order by random() LIMIT " + limit + ";";

            NpgsqlParameter datemin = new NpgsqlParameter();
            datemin.ParameterName = "@datemin";
            datemin.Value = DateTime.Now.AddMonths(-12);

            using (DbDataReader myR = myDC.GetFreesmsReaderObject(sql, datemin))
            {
                while (myR.Read())
                {
                    ReportRequestObject ro = FillReportRequestObject(myR);
                    reportsQueue.Enqueue(ro);
                    reportList.Add(ro);
                }

            }

            return reportList;
        }

        public static DbDataReader reportQueryReader(ReportRequestObject ro, Boolean cancelReport)
        {
            if (cancelReport)
                return null;

            String query = "SELECT ID, ID_USER, RECEIVED_DATETIME, BILLED_DATETIME, DELIVERED_DATETIME, SENT, ID_SUBACCOUNT, REQUESTED_GATEWAY, ";
            query += "REAL_GATEWAY, BASE_COST, FEE_COST, REAL_SMS_COST, REAL_SMS_FEE, LCR_OPERATOR, REAL_OPERATOR, RECEIVED_PROTOCOL, DELIVERY_REQUEST, ";
            query += "FLASH_REQUEST, SENDER, TEXT, IP_ADDRESS, PRIORITY, DESTINATION, BILLED, DELAYED_DELIVERY, MESSAGE_ID, APPLIED_RULE, DELIVERY_STATUS, ";
            query += "DELIVERY_DATETIME, NOTIFY_TO_CUSTOMER, CUSTOMER_MESSAGE_ID, LAST_NOTIFICATION_ATTEMPT, NUM_NOTIFICATION_ATTEMPTS ";
            //Boolean subAccount = isSubAccount(userName);
            String whereUser = "";
            if (ro.IsSubAccount)
                whereUser = "LOWER(id_user) = @user ";
            else
                whereUser = "LOWER(id_subaccount) = @user ";
            query += "FROM a_smsarchive WHERE " + whereUser + " AND ";
            query += "DELIVERED_DATETIME >= @startDate AND DELIVERED_DATETIME <= @endDate ";
            if (!String.IsNullOrEmpty(ro.ReportSenderParam)) query += "AND SENDER = @sender ";
            if (!String.IsNullOrEmpty(ro.ReportDestinyParam)) query += "AND DESTINATION = @destination ";
            query += " AND SENT < 10000 ";
            query += "ORDER BY DELIVERED_DATETIME;";

            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>();

            NpgsqlParameter user = new NpgsqlParameter();
            user.ParameterName = "@user";
            user.Value = ro.ReportLoginParam;
            parameters.Add(user);

            NpgsqlParameter startDate = new NpgsqlParameter();
            startDate.ParameterName = "@startDate";
            startDate.Value = ro.ReportStartDatetime;
            parameters.Add(startDate);

            NpgsqlParameter endDate = new NpgsqlParameter();
            endDate.ParameterName = "@endDate";
            endDate.Value = ro.ReportEndDatetime;
            parameters.Add(endDate);

            if (!String.IsNullOrEmpty(ro.ReportSenderParam))
            {
                NpgsqlParameter senderParam = new NpgsqlParameter();
                senderParam.ParameterName = "@sender";
                senderParam.Value = ro.ReportSenderParam;
                parameters.Add(senderParam);
            }

            if (!String.IsNullOrEmpty(ro.ReportDestinyParam))
            {
                NpgsqlParameter destinationParam = new NpgsqlParameter();
                destinationParam.ParameterName = "@destination";
                destinationParam.Value = ro.ReportDestinyParam;
                parameters.Add(destinationParam);
            }

            dbSmsdriverArchivio myDC = new dbSmsdriverArchivio();

            return myDC.GetSmsdriverArchivioReaderObjectPg(query, parameters.ToArray());
        }

        public static int UpdateReport(List<long> idReportList, ReportStatusDb status, int tempoElaborazione)
        {
            if (idReportList.Count == 0)
                return 0;

            dbFreesmsMiddlePg myDC = new dbFreesmsMiddlePg();
            try
            {
                String idList = String.Join(",", idReportList);
                List<NpgsqlParameter> parameterList = new List<NpgsqlParameter>();

                NpgsqlParameter tempoParam = new NpgsqlParameter();
                tempoParam.ParameterName = "@tempoElaborazione";
                tempoParam.Value = tempoElaborazione;
                parameterList.Add(tempoParam);

                NpgsqlParameter stateParam = new NpgsqlParameter();
                stateParam.ParameterName = "@flagSate";
                stateParam.Value = reportStatusDbToDescription(status);
                parameterList.Add(stateParam);

                string query = "update smsreport set report_TempoElaborazione=@tempoElaborazione, ";
                query += "report_Flagstate=@flagSate,report_SentDateTime=Now() where report_id in (" + idList + ");";

                using (NpgsqlCommand commandUpdate = myDC.GetFreesmsCommandObject(query, parameterList.ToArray()))
                {
                    return commandUpdate.ExecuteNonQuery();
                }
            }
            finally
            {
                try
                {
                    myDC.CloseFreesmsConnectionObject(myDC.GetFreesmsConnectionObject());
                }
                catch (Exception exc)
                {
                    logger.Fatal(exc.Message, exc);
                }
            }

        }

        public static List<ArchiveObject> reportQuery(ReportRequestObject ro, Boolean cancelReport)
        {
            List<ArchiveObject> archiveList = new List<ArchiveObject>();

            if (cancelReport)
                return null;

            String query = "SELECT ID, ID_USER, RECEIVED_DATETIME, BILLED_DATETIME, DELIVERED_DATETIME, SENT, ID_SUBACCOUNT, REQUESTED_GATEWAY, ";
            query += "REAL_GATEWAY, BASE_COST, FEE_COST, REAL_SMS_COST, REAL_SMS_FEE, LCR_OPERATOR, REAL_OPERATOR, RECEIVED_PROTOCOL, DELIVERY_REQUEST, ";
            query += "FLASH_REQUEST, SENDER, TEXT, IP_ADDRESS, PRIORITY, DESTINATION, BILLED, DELAYED_DELIVERY, MESSAGE_ID, APPLIED_RULE, DELIVERY_STATUS, ";
            query += "DELIVERY_DATETIME, NOTIFY_TO_CUSTOMER, CUSTOMER_MESSAGE_ID, LAST_NOTIFICATION_ATTEMPT, NUM_NOTIFICATION_ATTEMPTS ";
            String whereUser = "";
            if (ro.IsSubAccount)
                whereUser = "LOWER(id_user) = @user ";
            else
                whereUser = "LOWER(id_subaccount) = @user ";
            query += "FROM a_smsarchive WHERE " + whereUser + " AND ";
            query += "DELIVERED_DATETIME >= @startDate AND DELIVERED_DATETIME <= @endDate ";
            if(!String.IsNullOrEmpty(ro.ReportSenderParam)) query += "AND SENDER = @sender ";
            if(!String.IsNullOrEmpty(ro.ReportDestinyParam)) query += "AND DESTINATION = @destination ";
            query += " AND SENT < 10000 ";
            query += "ORDER BY DELIVERED_DATETIME;";

            List<NpgsqlParameter> parameters = new List<NpgsqlParameter>();

            NpgsqlParameter user = new NpgsqlParameter();
            user.ParameterName = "@user";
            user.Value = ro.ReportLoginParam;
            parameters.Add(user);

            NpgsqlParameter startDate = new NpgsqlParameter();
            startDate.ParameterName = "@startDate";
            startDate.Value = ro.ReportStartDatetime;
            parameters.Add(startDate);

            NpgsqlParameter endDate = new NpgsqlParameter();
            endDate.ParameterName = "@endDate";
            endDate.Value = ro.ReportEndDatetime;
            parameters.Add(endDate);

            if(!String.IsNullOrEmpty(ro.ReportSenderParam))
            {
                NpgsqlParameter senderParam = new NpgsqlParameter();
                senderParam.ParameterName = "@sender";
                senderParam.Value = ro.ReportSenderParam;
                parameters.Add(senderParam);
            }

            if(!String.IsNullOrEmpty(ro.ReportDestinyParam))
            {
                NpgsqlParameter destinationParam = new NpgsqlParameter();
                destinationParam.ParameterName = "@destination";
                destinationParam.Value = ro.ReportDestinyParam;
                parameters.Add(destinationParam);
            }

            dbSmsdriverArchivio myDC = new dbSmsdriverArchivio();

            using (DbDataReader myR = myDC.GetSmsdriverArchivioReaderObjectPg(query, parameters.ToArray()))
            {
                while (myR.Read())
                {
                    if (cancelReport)
                        return null;
                    archiveList.Add(FillArchiveObject(myR));
                }
            }

            return archiveList;

        }

        public static String DammiPaeseIso3(string login, Boolean isSubAccount)
        {
            String PaeseIso3 = "";

            if (!isSubAccount)
            {
                dbFreesmsMiddlePg dbx = new dbFreesmsMiddlePg();
                NpgsqlParameter userParam = new NpgsqlParameter();
                userParam.Value = login;
                userParam.ParameterName = "@userName";

                using (NpgsqlDataReader read = dbx.GetFreesmsReaderObject("SELECT Paese FROM smsAccount WHERE LOGIN = @userName;", userParam))
                {
                    if (read.Read())
                    {
                        PaeseIso3 = read["Paese"].ToString();
                    }
                }
            }
            else
            {
                String ncUpper = DammiUpperNC(login);
                dbFreesmsMiddlePg dbx = new dbFreesmsMiddlePg();
                NpgsqlParameter ncParam = new NpgsqlParameter();
                ncParam.Value = ncUpper;
                ncParam.ParameterName = "@NC";
                using (NpgsqlDataReader read = dbx.GetFreesmsReaderObject("SELECT Paese FROM smsAccount WHERE NC = @NC;", ncParam))
                {
                    if (read.Read())
                    {
                        PaeseIso3 = read["Paese"].ToString();
                    }
                }
            }

            return PaeseIso3;
        }

        private static String DammiUpperNC(string Uid)
        {
            String NCUpper = "";
            dbFreesmsMiddlePg dbx = new dbFreesmsMiddlePg();

            NpgsqlParameter userParam = new NpgsqlParameter();
            userParam.Value = Uid;
            userParam.ParameterName = "@userName";

            using (NpgsqlDataReader read = dbx.GetFreesmsReaderObject("SELECT NCUPPER FROM smsAccount WHERE LOGIN = @userName;", userParam))
            {
                if (read.Read())
                {
                    NCUpper = read["NCUPPER"].ToString();
                }
            }

            return NCUpper;
        }

        public static ArchiveObject FillArchiveObject(DbDataReader myR)
        {
            ArchiveObject ao = new ArchiveObject();
            ao.Id = Convert.ToInt32(myR["id"].ToString());
            ao.IdUser = myR["id_user"].ToString();
            ao.ReceivedDateTime = myR.GetDateTime(myR.GetOrdinal("received_datetime"));
            ao.BilledDateTime = myR.IsDBNull(myR.GetOrdinal("billed_datetime"))
                ? DateTime.MinValue : myR.GetDateTime(myR.GetOrdinal("billed_datetime"));
            ao.DeliveredDateTime = myR.IsDBNull(myR.GetOrdinal("delivered_datetime"))
                ? DateTime.MinValue : myR.GetDateTime(myR.GetOrdinal("delivered_datetime"));
            ao.Sent = myR.IsDBNull(myR.GetOrdinal("sent")) ? -1 : Convert.ToInt32(myR["sent"].ToString());
            ao.IdSubaccount = myR["id_subaccount"].ToString();
            ao.RequestedGateway = myR.IsDBNull(myR.GetOrdinal("requested_gateway")) ? -1 : Convert.ToInt32(myR["requested_gateway"].ToString());
            ao.RealGateway = myR.IsDBNull(myR.GetOrdinal("real_gateway")) ? -1 : Convert.ToInt32(myR["real_gateway"].ToString());
            ao.BaseCost = myR.IsDBNull(myR.GetOrdinal("base_cost")) ? -1 : Convert.ToInt32(myR["base_cost"].ToString());
            ao.FeeCost = myR.IsDBNull(myR.GetOrdinal("fee_cost")) ? -1 : Convert.ToInt32(myR["fee_cost"].ToString());
            ao.RealSmsCost = myR.IsDBNull(myR.GetOrdinal("real_sms_cost")) ? -1 : Convert.ToInt32(myR["real_sms_cost"].ToString());
            ao.RealSmsFee = myR.IsDBNull(myR.GetOrdinal("real_sms_fee")) ? -1 : Convert.ToInt32(myR["real_sms_fee"].ToString());
            ao.LcrOperator = myR.IsDBNull(myR.GetOrdinal("lcr_operator")) ? "" : myR["lcr_operator"].ToString();
            ao.RealOperator = myR.IsDBNull(myR.GetOrdinal("real_operator")) ? "" : myR["real_operator"].ToString();
            ao.ReceivedProtocol = myR.IsDBNull(myR.GetOrdinal("received_protocol")) ? -1 : Convert.ToInt32(myR["received_protocol"].ToString());
            ao.FlashRequest = myR.IsDBNull(myR.GetOrdinal("flash_request")) ? -1 : Convert.ToInt16(myR["flash_request"].ToString());
            ao.Sender = myR.IsDBNull(myR.GetOrdinal("sender")) ? "" : myR["sender"].ToString();
            ao.Text = myR.IsDBNull(myR.GetOrdinal("text")) ? "" : myR["text"].ToString();
            ao.IpAddress = myR.IsDBNull(myR.GetOrdinal("ip_address")) ? "" : myR["ip_address"].ToString();
            ao.Priority = myR.IsDBNull(myR.GetOrdinal("priority")) ? -1 : Convert.ToInt16(myR["priority"].ToString());
            ao.Destination = myR.IsDBNull(myR.GetOrdinal("destination")) ? "" : myR["destination"].ToString();
            ao.Billed = myR.IsDBNull(myR.GetOrdinal("billed")) ? -1 : Convert.ToInt16(myR["billed"].ToString());
            ao.DelayedDelivery = myR.IsDBNull(myR.GetOrdinal("delayed_delivery"))
                ? DateTime.MinValue : myR.GetDateTime(myR.GetOrdinal("delayed_delivery"));
            ao.MessageId = myR.IsDBNull(myR.GetOrdinal("message_id")) ? "" : myR["message_id"].ToString();
            ao.AppliedRule = Convert.ToInt32(myR["applied_rule"].ToString());
            ao.DeliveryStatus = myR.IsDBNull(myR.GetOrdinal("delivery_status")) ? -1 : Convert.ToInt32(myR["delivery_status"].ToString());
            ao.DeliveryDateTime = myR.IsDBNull(myR.GetOrdinal("delivery_datetime"))
                ? DateTime.MinValue : myR.GetDateTime(myR.GetOrdinal("delivery_datetime"));
            ao.NotifyToCustomer = myR.IsDBNull(myR.GetOrdinal("notify_to_customer")) ? -1 : Convert.ToInt16(myR["notify_to_customer"].ToString());
            ao.CustomerMessageId = myR.IsDBNull(myR.GetOrdinal("customer_message_id"))
                ? "" : myR["customer_message_id"].ToString();
            ao.LastNotificationAttempts = myR.IsDBNull(myR.GetOrdinal("last_notification_attempt"))
                ? DateTime.MinValue : myR.GetDateTime(myR.GetOrdinal("last_notification_attempt"));
            ao.NumNotificationAttempts = myR.IsDBNull(myR.GetOrdinal("num_notification_attempts"))
                ? -1 : Convert.ToInt32(myR["num_notification_attempts"].ToString());

            return ao;
        }

        private static bool isSubAccount(string Uid)
        {
            dbFreesmsMiddlePg myDC = new dbFreesmsMiddlePg();
            Boolean isSubaccount = false;

            NpgsqlParameter userParam = new NpgsqlParameter();
            userParam.Value = Uid.ToLower();
            userParam.ParameterName = "@userName";

            using (NpgsqlDataReader read = myDC.GetFreesmsReaderObject("SELECT smssubaccount FROM smsAccount WHERE LOWER(login) = @userName;", userParam))
            {
                if (read.Read())
                {
                    if (read["smssubaccount"].ToString() == "N")
                    {
                        isSubaccount = false;
                    }
                    else
                    {
                        isSubaccount = true;
                    }
                }
            }

            return isSubaccount;
        }

        private static string reportStatusDbToDescription(ReportStatusDb val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType()
                .GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }

    }

    public enum ReportStatusDb
    {
        [Description("N")]
        DaElaborare = 1,
        [Description("X")]
        InElaborazione = 2,
        [Description("S")]
        ElaboratoOK = 3,
        [Description("E")]
        ElaboratoKO = 4
    }
}
