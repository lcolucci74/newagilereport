﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewAgileReport.Utils
{
    class QuartzUtil
    {
        public static String ProssimaEsecuzione(IJobDetail job, IScheduler scheduler)
        {
            try
            {
                DateTime ft = DateTime.MinValue;
                var triggers = scheduler.GetTriggersOfJob(job.Key);
                var nextFireTimeUtc = triggers[0].GetNextFireTimeUtc();
                ft = TimeZone.CurrentTimeZone.ToLocalTime(nextFireTimeUtc.Value.DateTime);
                return string.Format("{0} Prossima esecuzione a : {1}", job.Key, ft.ToString("dd/MM/yyyy HH:mm:ss"));
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static DateTime ProssimaEsecuzioneDateTime(IJobDetail job, IScheduler scheduler)
        {
            try
            {
                DateTime ft = DateTime.MinValue;
                var triggers = scheduler.GetTriggersOfJob(job.Key);
                var nextFireTimeUtc = triggers[0].GetNextFireTimeUtc();
                return TimeZone.CurrentTimeZone.ToLocalTime(nextFireTimeUtc.Value.DateTime);
            }
            catch (Exception)
            {
                return DateTime.Now;
            }
        }
    }
}
