﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewAgileReport.Pojo
{
    class ArchiveObject
    {
        private long m_Id;
        private String m_IdUser;
        private DateTime m_ReceivedDateTime;
        private DateTime m_BilledDateTime;
        private DateTime m_DeliveredDateTime;
        private int m_Sent;
        private String m_IdSubaccount;
        private int m_RequestedGateway;
        private int m_RealGateway;
        private int m_BaseCost;
        private int m_FeeCost;
        private int m_RealSmsCost;
        private int m_RealSmsFee;
        private String m_LcrOperator;
        private String m_RealOperator;
        private int m_ReceivedProtocol;
        private String m_DeliveryRequest;
        private int m_FlashRequest;
        private String m_Sender;
        private String m_Text;
        private String m_IpAddress;
        private int m_Priority;
        private String m_Destination;
        private int m_Billed;
        private DateTime m_DelayedDelivery;
        private String m_MessageId;
        private int m_AppliedRule;
        private int m_DeliveryStatus;
        private DateTime m_DeliveryDateTime;
        private int m_NotifyToCustomer;
        private String m_CustomerMessageId;
        private DateTime m_LastNotificationAttempts;
        private int m_NumNotificationAttempts;

        public long Id
        {
            get
            {
                return m_Id;
            }

            set
            {
                m_Id = value;
            }
        }

        public string IdUser
        {
            get
            {
                return m_IdUser;
            }

            set
            {
                m_IdUser = value;
            }
        }

        public DateTime ReceivedDateTime
        {
            get
            {
                return m_ReceivedDateTime;
            }

            set
            {
                m_ReceivedDateTime = value;
            }
        }

        public DateTime BilledDateTime
        {
            get
            {
                return m_BilledDateTime;
            }

            set
            {
                m_BilledDateTime = value;
            }
        }

        public DateTime DeliveredDateTime
        {
            get
            {
                return m_DeliveredDateTime;
            }

            set
            {
                m_DeliveredDateTime = value;
            }
        }

        public int Sent
        {
            get
            {
                return m_Sent;
            }

            set
            {
                m_Sent = value;
            }
        }

        public string IdSubaccount
        {
            get
            {
                return m_IdSubaccount;
            }

            set
            {
                m_IdSubaccount = value;
            }
        }

        public int RequestedGateway
        {
            get
            {
                return m_RequestedGateway;
            }

            set
            {
                m_RequestedGateway = value;
            }
        }

        public int RealGateway
        {
            get
            {
                return m_RealGateway;
            }

            set
            {
                m_RealGateway = value;
            }
        }

        public int BaseCost
        {
            get
            {
                return m_BaseCost;
            }

            set
            {
                m_BaseCost = value;
            }
        }

        public int FeeCost
        {
            get
            {
                return m_FeeCost;
            }

            set
            {
                m_FeeCost = value;
            }
        }

        public int RealSmsCost
        {
            get
            {
                return m_RealSmsCost;
            }

            set
            {
                m_RealSmsCost = value;
            }
        }

        public int RealSmsFee
        {
            get
            {
                return m_RealSmsFee;
            }

            set
            {
                m_RealSmsFee = value;
            }
        }

        public string LcrOperator
        {
            get
            {
                return m_LcrOperator;
            }

            set
            {
                m_LcrOperator = value;
            }
        }

        public string RealOperator
        {
            get
            {
                return m_RealOperator;
            }

            set
            {
                m_RealOperator = value;
            }
        }

        public int ReceivedProtocol
        {
            get
            {
                return m_ReceivedProtocol;
            }

            set
            {
                m_ReceivedProtocol = value;
            }
        }

        public string DeliveryRequest
        {
            get
            {
                return m_DeliveryRequest;
            }

            set
            {
                m_DeliveryRequest = value;
            }
        }

        public int FlashRequest
        {
            get
            {
                return m_FlashRequest;
            }

            set
            {
                m_FlashRequest = value;
            }
        }

        public string Sender
        {
            get
            {
                return m_Sender;
            }

            set
            {
                m_Sender = value;
            }
        }

        public string Text
        {
            get
            {
                return m_Text;
            }

            set
            {
                m_Text = value;
            }
        }

        public string IpAddress
        {
            get
            {
                return m_IpAddress;
            }

            set
            {
                m_IpAddress = value;
            }
        }

        public int Priority
        {
            get
            {
                return m_Priority;
            }

            set
            {
                m_Priority = value;
            }
        }

        public string Destination
        {
            get
            {
                return m_Destination;
            }

            set
            {
                m_Destination = value;
            }
        }

        public int Billed
        {
            get
            {
                return m_Billed;
            }

            set
            {
                m_Billed = value;
            }
        }

        public DateTime DelayedDelivery
        {
            get
            {
                return m_DelayedDelivery;
            }

            set
            {
                m_DelayedDelivery = value;
            }
        }

        public string MessageId
        {
            get
            {
                return m_MessageId;
            }

            set
            {
                m_MessageId = value;
            }
        }

        public int AppliedRule
        {
            get
            {
                return m_AppliedRule;
            }

            set
            {
                m_AppliedRule = value;
            }
        }

        public int DeliveryStatus
        {
            get
            {
                return m_DeliveryStatus;
            }

            set
            {
                m_DeliveryStatus = value;
            }
        }

        public DateTime DeliveryDateTime
        {
            get
            {
                return m_DeliveryDateTime;
            }

            set
            {
                m_DeliveryDateTime = value;
            }
        }

        public int NotifyToCustomer
        {
            get
            {
                return m_NotifyToCustomer;
            }

            set
            {
                m_NotifyToCustomer = value;
            }
        }

        public string CustomerMessageId
        {
            get
            {
                return m_CustomerMessageId;
            }

            set
            {
                m_CustomerMessageId = value;
            }
        }

        public DateTime LastNotificationAttempts
        {
            get
            {
                return m_LastNotificationAttempts;
            }

            set
            {
                m_LastNotificationAttempts = value;
            }
        }

        public int NumNotificationAttempts
        {
            get
            {
                return m_NumNotificationAttempts;
            }

            set
            {
                m_NumNotificationAttempts = value;
            }
        }
    }
}
