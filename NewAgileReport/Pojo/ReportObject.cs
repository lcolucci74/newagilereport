﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewAgileReport.Pojo
{
    public class ReportRequestObject
    {
        /*
         * 
         * report_id bigserial NOT NULL,
          report_requestdatetime timestamp without time zone DEFAULT now(),
          report_loginparam citext DEFAULT ''::character varying,
          report_startdatetime timestamp without time zone,
          report_enddatetime timestamp without time zone,
          report_senderparam citext DEFAULT ''::character varying,
          report_destinyparam citext DEFAULT ''::character varying,
          report_email citext,
          report_flagstate citext DEFAULT 'N'::character varying,
          report_sentdatetime timestamp without time zone,
          report_tempoelaborazione integer,
          report_campaign_id integer,
         * 
         * 
         * */

        private long m_ReportId;
        private DateTime m_ReportRequestdatetime;
        private String m_ReportLoginParam;
        private DateTime m_ReportStartDatetime;
        private DateTime m_ReportEndDatetime;
        private String m_ReportSenderParam;
        private String m_ReportDestinyParam;
        private String m_ReportEmail;
        private String m_ReportFlagstate;
        private DateTime m_ReportSentdatetime;
        private int m_ReportTempoelaborazione;
        private long m_ReportCampaignId;
        private Boolean m_IsSubAccount;
        private String m_PaeseIso3;

        public string ReportLoginParam
        {
            get
            {
                return m_ReportLoginParam;
            }

            set
            {
                m_ReportLoginParam = value;
            }
        }

        public long ReportId
        {
            get
            {
                return m_ReportId;
            }

            set
            {
                m_ReportId = value;
            }
        }

        public DateTime ReportRequestdatetime
        {
            get
            {
                return m_ReportRequestdatetime;
            }

            set
            {
                m_ReportRequestdatetime = value;
            }
        }

        public DateTime ReportStartDatetime
        {
            get
            {
                return m_ReportStartDatetime;
            }

            set
            {
                m_ReportStartDatetime = value;
            }
        }

        public DateTime ReportEndDatetime
        {
            get
            {
                return m_ReportEndDatetime;
            }

            set
            {
                m_ReportEndDatetime = value;
            }
        }

        public string ReportSenderParam
        {
            get
            {
                return m_ReportSenderParam;
            }

            set
            {
                m_ReportSenderParam = value;
            }
        }

        public string ReportDestinyParam
        {
            get
            {
                return m_ReportDestinyParam;
            }

            set
            {
                m_ReportDestinyParam = value;
            }
        }

        public string ReportEmail
        {
            get
            {
                return m_ReportEmail;
            }

            set
            {
                m_ReportEmail = value;
            }
        }

        public string ReportFlagstate
        {
            get
            {
                return m_ReportFlagstate;
            }

            set
            {
                m_ReportFlagstate = value;
            }
        }

        public DateTime ReportSentdatetime
        {
            get
            {
                return m_ReportSentdatetime;
            }

            set
            {
                m_ReportSentdatetime = value;
            }
        }

        public int ReportTempoelaborazione
        {
            get
            {
                return m_ReportTempoelaborazione;
            }

            set
            {
                m_ReportTempoelaborazione = value;
            }
        }

        public long ReportCampaignId
        {
            get
            {
                return m_ReportCampaignId;
            }

            set
            {
                m_ReportCampaignId = value;
            }
        }

        public bool IsSubAccount
        {
            get
            {
                return m_IsSubAccount;
            }

            set
            {
                m_IsSubAccount = value;
            }
        }

        public string PaeseIso3
        {
            get
            {
                return m_PaeseIso3;
            }

            set
            {
                m_PaeseIso3 = value;
            }
        }
    }
}
