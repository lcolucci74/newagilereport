﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewAgileReport.Pojo
{
    public class SMSObject
    {
        private int m_countSms;
        private Double m_costSms;

        public int CountSms
        {
            get { return m_countSms; }
            set { m_countSms = value; }
        }
        public Double CostSms
        {
            get { return m_costSms; }
            set { m_costSms = value; }
        }

    }
}
