﻿using NewAgileReport.Pojo;
using NewAgileReport.Utils;
using NewAgileReport.Worker;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace NewAgileReport
{
    public class BackgroundReportMaker
    {
        private ExtendedBackgroundWorker _bw;
        //private Boolean _cancelReport;
        private String _reportName;

        public BackgroundReportMaker(String reportName)
        {
            _bw = new ExtendedBackgroundWorker("");
            _reportName = reportName;

        }

        public void SetWorkerCompletedEvent(RunWorkerCompletedEventHandler wch)
        {
            _bw.RunWorkerCompleted += wch;
        }

        public void SetWorkerProgressChangedEvent(ProgressChangedEventHandler pch)
        {
            _bw.WorkerReportsProgress = true;
            _bw.ProgressChanged += pch;
        }

        public void SetWorkerDoWorkEvent(DoWorkEventHandler dch)
        {
            _bw.DoWork += dch;
        }

        public String getReportName()
        {
            return _reportName;
        }

        public ExtendedBackgroundWorker getWorker()
        {
            return _bw;
        }

        public void killReport()
        {
            if (IsBusy())
                _bw.Abort();             
        }

        public void makeReport(ReportRequestObject ro)
        {
            try
            {
                if (!IsBusy())
                    _bw.RunWorkerAsync(ro);
            }
            catch (Exception ex)
            {
                //TODO
            }
                
        }

        public void stopReport()
        {
            if(IsBusy())
                _bw.CancelAsync();
        }

        public Boolean IsBusy()
        {
            return _bw.IsBusy;
        }

        //private void backgroundReportMaker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    try
        //    {
        //        ReportRequestObject ro = (ReportRequestObject)e.Argument;
        //        notificator(ActionDebug.UserName, _reportName, ro.ReportLoginParam);
        //        notificator(ActionDebug.DatagridStatus, _reportName, ((int)ReportStatus.Working).ToString());
        //        Random rnd = new Random(Guid.NewGuid().GetHashCode());
        //        int sleep = rnd.Next(50, 60);
        //        for (int i = 0; i < 100; i++)
        //        {
        //            if (_bw.CancellationPending)
        //            {
        //                e.Cancel = true;
        //                break;
        //            }
        //            int err = rnd.Next(0, 10001);
        //            if (err == 0)
        //                throw new Exception("Errorone : " + err);
        //            notificator(ActionDebug.DatagridInfo, _reportName, "ID : " + ro.ReportId + " : " + (i + 1));
        //            Thread.Sleep(sleep);
        //        }

        //        //notificator(ActionDebug.UserName, _reportName, ro.ReportLoginParam);
        //        //notificator(ActionDebug.DatagridInfo, _reportName, "IN LAVORAZIONE");
        //        //notificator(ActionDebug.DatagridStatus, _reportName, ((int)ReportStatus.Working).ToString());
        //        //Application.DoEvents();
        //        //List<ArchiveObject> ao = DbUtils.reportQuery(ro, _bw.CancellationPending);
        //        if (_bw.CancellationPending)
        //        {
        //            notificator(ActionDebug.DatagridInfo, _reportName, "CANCELLED BY USER");
        //            notificator(ActionDebug.DatagridStatus, _reportName, ((int)ReportStatus.Cancelled).ToString());
        //            e.Cancel = true;
        //        }                   
        //        else
        //        {
        //            notificator(ActionDebug.DatagridStatus, _reportName, ((int)ReportStatus.Completed).ToString());
        //            notificator(ActionDebug.DatagridInfo, _reportName, "ID : " + ro.ReportId + " COMPLETED");
        //            e.Result = ro;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        e.Result = ex;
        //        if (ex is ThreadAbortException)
        //        {
        //            notificator(ActionDebug.DatagridInfo, _reportName, "KILLED BY USER");
        //            notificator(ActionDebug.DatagridStatus, _reportName, ((int)ReportStatus.Cancelled).ToString());
        //            Application.DoEvents();
        //        }
        //        else
        //        {
        //            Exception error = (Exception)e.Result;
        //            notificator(ActionDebug.DatagridInfo, _reportName, "Error : " + error.Message);
        //            notificator(ActionDebug.DatagridStatus, _reportName, ((int)ReportStatus.DoWorkError).ToString());
        //            notificator(ActionDebug.Error, _reportName, "");
        //        }
                
        //    }
        //}

        //private void backgroundReportMaker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    if(e.Error is WorkerAbortException) 
        //        workerCompletedNotificator(ReportStatus.KilledAndStop);
        //    else if(e.Error != null)
        //        workerCompletedNotificator(ReportStatus.GeneralError);
        //    else if (e.Cancelled)
        //    {               
        //        workerCompletedNotificator(ReportStatus.CancelledAndStop);
        //    }
        //    else if (e.Result is Exception && !(e.Result is ThreadAbortException))
        //    {
                
        //        workerCompletedNotificator(ReportStatus.Completed);
        //    }
        //    else
        //    {
                
        //        workerCompletedNotificator(ReportStatus.Completed);
        //    }

        //}

        public static string reportStatusToDescription(ReportStatus val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType()
                .GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }

        public static ReportStatus stringToReportStatus(String statusString)
        {
            try
            {
                return (ReportStatus)Convert.ToInt16(statusString);
            }
            catch (Exception)
            {

                return ReportStatus.Timeout;
            }
        }
    }

    public enum ActionDebug
    {
        LogMessage = 1,
        DatagridInfo = 2,
        DatagridStatus = 3,
        UserName = 4,
        Error = 5,
        Duration = 6
    }

    public enum ReportStatus
    {
        [Description("Timeout")]
        Timeout = 1,
        [Description("Working")]
        Working = 2,
        [Description("Completed")]
        Completed = 3,
        [Description("Cancelled")]
        Cancelled = 4,
        [Description("Error")]
        DoWorkError = 5,
        [Description("CancelledAndStop")]
        CancelledAndStop = 6,
        [Description("Killed")]
        Killed = 7,
        [Description("KilledAndStop")]
        KilledAndStop = 8,
        [Description("GeneralError")]
        GeneralError = 9,
        [Description("Cancelling")]
        Cancelling = 10,
        [Description("Killing")]
        Killing = 11

    }
}
