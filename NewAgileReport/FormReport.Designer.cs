﻿namespace NewAgileReport
{
    partial class FormReport
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewReports = new System.Windows.Forms.DataGridView();
            this.labelQueueLength = new System.Windows.Forms.Label();
            this.buttonKillWorkers = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonFlush = new System.Windows.Forms.Button();
            this.comboBoxWorkers = new System.Windows.Forms.ComboBox();
            this.buttonStopWorkers = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.labelError = new System.Windows.Forms.Label();
            this.stato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.user = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lavorazione = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tempo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReports)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridViewReports
            // 
            this.dataGridViewReports.AllowUserToAddRows = false;
            this.dataGridViewReports.AllowUserToDeleteRows = false;
            this.dataGridViewReports.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewReports.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.stato,
            this.user,
            this.lavorazione,
            this.tempo});
            this.dataGridViewReports.Location = new System.Drawing.Point(12, 277);
            this.dataGridViewReports.Name = "dataGridViewReports";
            this.dataGridViewReports.ReadOnly = true;
            this.dataGridViewReports.Size = new System.Drawing.Size(753, 254);
            this.dataGridViewReports.TabIndex = 0;
            this.dataGridViewReports.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewReports_CellFormatting);
            this.dataGridViewReports.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewReports_CellValueChanged);
            // 
            // labelQueueLength
            // 
            this.labelQueueLength.AutoSize = true;
            this.labelQueueLength.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQueueLength.Location = new System.Drawing.Point(160, 10);
            this.labelQueueLength.Name = "labelQueueLength";
            this.labelQueueLength.Size = new System.Drawing.Size(30, 31);
            this.labelQueueLength.TabIndex = 3;
            this.labelQueueLength.Text = "0";
            // 
            // buttonKillWorkers
            // 
            this.buttonKillWorkers.Location = new System.Drawing.Point(8, 48);
            this.buttonKillWorkers.Name = "buttonKillWorkers";
            this.buttonKillWorkers.Size = new System.Drawing.Size(75, 23);
            this.buttonKillWorkers.TabIndex = 4;
            this.buttonKillWorkers.Text = "KILL";
            this.buttonKillWorkers.UseVisualStyleBackColor = true;
            this.buttonKillWorkers.Click += new System.EventHandler(this.button_kill_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxLog.Location = new System.Drawing.Point(12, 145);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(753, 126);
            this.richTextBoxLog.TabIndex = 66;
            this.richTextBoxLog.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonFlush);
            this.panel1.Controls.Add(this.comboBoxWorkers);
            this.panel1.Controls.Add(this.buttonStopWorkers);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.labelQueueLength);
            this.panel1.Controls.Add(this.buttonKillWorkers);
            this.panel1.Location = new System.Drawing.Point(444, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(321, 112);
            this.panel1.TabIndex = 67;
            // 
            // buttonFlush
            // 
            this.buttonFlush.Location = new System.Drawing.Point(233, 13);
            this.buttonFlush.Name = "buttonFlush";
            this.buttonFlush.Size = new System.Drawing.Size(75, 23);
            this.buttonFlush.TabIndex = 7;
            this.buttonFlush.Text = "FLUSH";
            this.buttonFlush.UseVisualStyleBackColor = true;
            this.buttonFlush.Click += new System.EventHandler(this.buttonFlush_Click);
            // 
            // comboBoxWorkers
            // 
            this.comboBoxWorkers.FormattingEnabled = true;
            this.comboBoxWorkers.Location = new System.Drawing.Point(106, 62);
            this.comboBoxWorkers.Name = "comboBoxWorkers";
            this.comboBoxWorkers.Size = new System.Drawing.Size(141, 21);
            this.comboBoxWorkers.TabIndex = 6;
            // 
            // buttonStopWorkers
            // 
            this.buttonStopWorkers.Location = new System.Drawing.Point(8, 78);
            this.buttonStopWorkers.Name = "buttonStopWorkers";
            this.buttonStopWorkers.Size = new System.Drawing.Size(75, 23);
            this.buttonStopWorkers.TabIndex = 5;
            this.buttonStopWorkers.Text = "STOP";
            this.buttonStopWorkers.UseVisualStyleBackColor = true;
            this.buttonStopWorkers.Click += new System.EventHandler(this.buttonStopWorkers_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Report in coda";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(12, 12);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 68;
            this.buttonStart.Text = "START";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(12, 60);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 69;
            this.buttonStop.Text = "STOP";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 25);
            this.label1.TabIndex = 7;
            this.label1.Text = "Errori";
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelError.Location = new System.Drawing.Point(99, 93);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(30, 31);
            this.labelError.TabIndex = 7;
            this.labelError.Text = "0";
            // 
            // stato
            // 
            this.stato.HeaderText = "Stato";
            this.stato.Name = "stato";
            this.stato.ReadOnly = true;
            // 
            // user
            // 
            this.user.HeaderText = "User";
            this.user.Name = "user";
            this.user.ReadOnly = true;
            this.user.Width = 150;
            // 
            // lavorazione
            // 
            this.lavorazione.HeaderText = "Lavorazione";
            this.lavorazione.Name = "lavorazione";
            this.lavorazione.ReadOnly = true;
            this.lavorazione.Width = 280;
            // 
            // tempo
            // 
            this.tempo.HeaderText = "Tempo";
            this.tempo.Name = "tempo";
            this.tempo.ReadOnly = true;
            // 
            // FormReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 543);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.dataGridViewReports);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormReport";
            this.Text = "NewReportSender";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormReport_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormReport_FormClosed);
            this.Load += new System.EventHandler(this.FormReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewReports)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewReports;
        private System.Windows.Forms.Label labelQueueLength;
        private System.Windows.Forms.Button buttonKillWorkers;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonStopWorkers;
        private System.Windows.Forms.ComboBox comboBoxWorkers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.Button buttonFlush;
        private System.Windows.Forms.DataGridViewTextBoxColumn stato;
        private System.Windows.Forms.DataGridViewTextBoxColumn user;
        private System.Windows.Forms.DataGridViewTextBoxColumn lavorazione;
        private System.Windows.Forms.DataGridViewTextBoxColumn tempo;
    }
}

