﻿using NewAgileReport.Pojo;
using NewAgileReport.Utils;
using NewAgileReport.Utils.Queue;
using NewAgileReport.Utils.Queue.System.Collections.Concurrent;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NewAgileReport
{
    public partial class FormReport : Form
    {

        public delegate void TheNotificatorInvoked(ActionDebug action, String reportName, String message);
        public TheNotificatorInvoked notificatorInvoked;
        public delegate void QueueChangedInvoked(int queueSize);
        public QueueChangedInvoked queueChangedInvoked;
        //private List<BackgroundReportMaker> reportList = new List<BackgroundReportMaker>();
        //private ObservableConcurrentQueue<ReportObject> reportsQueue = null;
        public delegate void ProduceReportInvoked(JobMessages jobMessages);
        public ProduceReportInvoked produceReportInvoked;

        private Boolean _cancelledEvent = false;

        public FormReport()
        {
            InitializeComponent();
            //Double buufered datagrid
            typeof(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic
                | BindingFlags.Instance | BindingFlags.SetProperty,
                null, dataGridViewReports, new object[] { true });
        }

        private void FormReport_Load(object sender, EventArgs e)
        {
            //Verifico la correttezza della configurazione
            ReportUtils.CheckConfigParams();
            //Disabilito ordinamento
            dataGridViewReports.Columns.Cast<DataGridViewColumn>()
                .ToList().ForEach(f => f.SortMode = DataGridViewColumnSortMode.NotSortable);
            notificatorInvoked = new TheNotificatorInvoked(myNotificator);
            queueChangedInvoked = new QueueChangedInvoked(myQueueChanged);
            produceReportInvoked = new ProduceReportInvoked(myProduceReport);
            ReportSenderJob.baseQueueChanged += new ReportSenderJob.QueueChangedHandler(queueChanged);
            ReportSenderJob.baseProduceReport += new ReportSenderJob.ProduceReportHandler(produceReport);
            ReportSenderJob.baseNotify += new ReportSenderJob.NotifyHandler(notificator);
            int workersListSize = Convert.ToInt16(ConfigurationManager.AppSettings["WorkersNumber"]);
            ReportSenderJob.CreateWorkersList(workersListSize);
            for (int i=0;i< workersListSize; i++)
            {
                Object[] row = new Object[] { ReportStatus.Timeout, "", "", "" };
                int index = dataGridViewReports.Rows.Add(row);
                dataGridViewReports.Rows[index].HeaderCell.Value = ReportSenderJob.getWorkerName(i);
                dataGridViewReports.RowHeadersWidth = 100;
                //reportList.Add(report);
                comboBoxWorkers.Items.Add(ReportSenderJob.getWorkerName(i));
            }
        }

        private void notificator(ActionDebug action, String reportName, String message)
        {
            Invoke(notificatorInvoked, new Object[] { action, reportName, message });
        }

        private void IncrementError()
        {
            int err = Convert.ToInt32(labelError.Text);
            err++;
            labelError.Text = "" + err;

            if (err >= 10000)
            {
                buttonStop_Click(null, null);
                //button_kill_Click(null, null);
                MessageBox.Show("Applicazione ferma per troppi errori", "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void queueChanged(int queueSize)
        {
            Invoke(queueChangedInvoked, queueSize);
        }

        private void myQueueChanged(int queueSize)
        {
            labelQueueLength.Text = "" + queueSize;
            Application.DoEvents();
        }

        private void myNotificator(ActionDebug action, String reportName, String message)
        {
            int indexReport = ReportSenderJob.getWorkerReportIndex(reportName);
            if (indexReport >= 0)
            {
                switch (action)
                {
                    case ActionDebug.Error:
                        if (!_cancelledEvent)
                            IncrementError();
                        break;
                    case ActionDebug.DatagridInfo:
                        //richTextBoxLog.AppendText(message + Environment.NewLine);
                        dataGridViewReports.Rows[indexReport].Cells["lavorazione"].Value = message;
                        break;
                    case ActionDebug.Duration:
                        //richTextBoxLog.AppendText(message + Environment.NewLine);
                        dataGridViewReports.Rows[indexReport].Cells["tempo"].Value = message;
                        break;
                    case ActionDebug.UserName:
                        //richTextBoxLog.AppendText(message + Environment.NewLine);
                        dataGridViewReports.Rows[indexReport].Cells["user"].Value = message;
                        break;
                    case ActionDebug.DatagridStatus:
                        ReportStatus reposrtStatus = BackgroundReportMaker.stringToReportStatus(message);
                        dataGridViewReports.Rows[indexReport].Cells["stato"].Value = BackgroundReportMaker.reportStatusToDescription(reposrtStatus);
                        Application.DoEvents();
                        switch (reposrtStatus)
                        {
                            case ReportStatus.Timeout:
                                dataGridViewReports.Rows[indexReport].DefaultCellStyle.BackColor = Color.White;
                                break;
                            case ReportStatus.Working:
                                dataGridViewReports.Rows[indexReport].DefaultCellStyle.BackColor = Color.LightGreen;
                                break;
                            case ReportStatus.Completed:
                                dataGridViewReports.Rows[indexReport].DefaultCellStyle.BackColor = Color.White;
                                break;
                            case ReportStatus.Cancelled:
                                dataGridViewReports.Rows[indexReport].DefaultCellStyle.BackColor = Color.OrangeRed;
                                break;
                            case ReportStatus.Cancelling:
                            case ReportStatus.Killing:
                                dataGridViewReports.Rows[indexReport].DefaultCellStyle.BackColor = Color.Yellow;
                                break;
                            case ReportStatus.DoWorkError:
                                dataGridViewReports.Rows[indexReport].DefaultCellStyle.BackColor = Color.Red;
                                break;

                        }
                        Application.DoEvents();
                        break;
                    case ActionDebug.LogMessage:
                        if (richTextBoxLog.Lines.Count() > 100)
                            richTextBoxLog.Text = "";
                        richTextBoxLog.AppendText(message + Environment.NewLine);
                        richTextBoxLog.ScrollToCaret();
                        richTextBoxLog.Refresh();
                        break;

                }
            }
            Application.DoEvents();

        }

        private void button_kill_Click(object sender, EventArgs e)
        {
            _cancelledEvent = true;
            buttonStop_Click(null, null);
            ReportSenderJob.KillAll();
        }

        private ISchedulerFactory reportSenderSchedulerFactory = null;
        private IScheduler reportSenderScheduler;
        private IJobDetail reportSenderJob;
        private ITrigger reportSenderTrigger;

        private void buttonStart_Click(object sender, EventArgs e)
        {
            NameValueCollection properties = new NameValueCollection();
            properties["quartz.scheduler.instanceName"] = "ReportSenderScheduler";

            reportSenderSchedulerFactory = new StdSchedulerFactory(properties);
            reportSenderScheduler = reportSenderSchedulerFactory.GetScheduler();

            if (!reportSenderScheduler.IsStarted)
            {

                buttonStart.Enabled = false;
                buttonStop.Enabled = true;
                _cancelledEvent = false;

                reportSenderJob = JobBuilder.Create<ReportSenderJob>()
                            .WithIdentity("ReportSenderJob", "reportSenderGroup")
                            .Build();

                reportSenderTrigger = TriggerBuilder.Create()
                    .WithIdentity("ReportSenderTrigger", "reportSenderGroup")
                    .StartNow()
                    .WithSimpleSchedule(x => x.WithIntervalInSeconds(60).RepeatForever())
                    .Build();

                DateTimeOffset ft = reportSenderScheduler.ScheduleJob(reportSenderJob, reportSenderTrigger);

                labelError.Text = "0";

                reportSenderScheduler.Start();

            }
        }

        private DateTime nextExecutionReport = DateTime.Now;

        private void produceReport(JobMessages jobMessages)
        {
            Invoke(produceReportInvoked, new Object[] { jobMessages });
        }

        private void myProduceReport(JobMessages jobMessages)
        {
            nextExecutionReport = QuartzUtil.ProssimaEsecuzioneDateTime(reportSenderJob, reportSenderScheduler);
            if(jobMessages.Errors == null)
            {
                richTextBoxLog.AppendText(DateTime.Now + ": Trovati : " + jobMessages.ReportList.Count + " report" + Environment.NewLine);
            }
                
            else
            {
                richTextBoxLog.AppendText(DateTime.Now + ": " + jobMessages.Errors.Message + Environment.NewLine);
                IncrementError();
            }
            Application.DoEvents();

        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (reportSenderScheduler != null && reportSenderScheduler.IsStarted)
            {
                buttonStop.Enabled = false;
                buttonStart.Enabled = true;

                reportSenderScheduler.Shutdown();
            }
        }

        private void FormReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void FormReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            buttonStop_Click(null, null);
            ReportSenderJob.KillAll();
        }

        private void buttonStopWorkers_Click(object sender, EventArgs e)
        {
            buttonStop_Click(null, null);
            _cancelledEvent = true;
            ReportSenderJob.StopAll();
        }

        private void buttonFlush_Click(object sender, EventArgs e)
        {
            ReportSenderJob.flushQueue();
        }

        private void dataGridViewReports_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dataGridViewReports_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
           
                
        }
    }
}
