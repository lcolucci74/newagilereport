﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewAgileReport.Worker
{
    class WorkerAbortException : Exception
    {
        public WorkerAbortException()
        {

        }

        public WorkerAbortException(string message) : base(message)
        {

        }

        public WorkerAbortException(string message, Exception inner) : base(message, inner)
        {

        }
    }

    class WorkerTimeOutException : Exception
    {
        public WorkerTimeOutException()
        {

        }

        public WorkerTimeOutException(string message) : base(message)
        {

        }

        public WorkerTimeOutException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
