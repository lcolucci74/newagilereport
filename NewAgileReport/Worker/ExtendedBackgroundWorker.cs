﻿using System;
using System.ComponentModel;
using System.Reflection;
using NewAgileReport.Worker;
using System.Threading;

namespace NewAgileReport
{
    public class ExtendedBackgroundWorker : BackgroundWorker
    {

        private Timer _timerTimeout;
        private DateTime _startWorkingDate;
        private DateTime _endWorkingDate;
        private TimeSpan _workingDuration;
        private Boolean _checkTimeout;
        private int _timeout;
        private Object lockObject = new object();
        public delegate void WorkerTimeOutEventHandler(object sender);
        public event WorkerTimeOutEventHandler WorkerTimeOutEvent;

        //public delegate void WorkerTimeOutEventHandler(Object sender, ElapsedEventArgs e);
        //public static event WorkerTimeOutEventHandler baseWorkerTimeOut;

        public ExtendedBackgroundWorker(String workerName, Boolean checkTimeout, int timeout)
        {
            if(checkTimeout)
            {
                _timerTimeout = new Timer(ElapsedEvent, null, Timeout.Infinite, Timeout.Infinite);
                //_timerTimeout.AutoReset = true;
                //_timerTimeout.Elapsed += new ElapsedEventHandler(ElapsedEvent);
                _timeout = timeout;
                _checkTimeout = checkTimeout;
            }          
            _workerName = workerName;
            ResetWorkingStartDate();
        }

        private void ResetWorkingStartDate()
        {
            lock(lockObject)
            {
                _startWorkingDate = DateTime.Now;
                _endWorkingDate = DateTime.Now;
            }
            
        }

        private void CalculateWorkingEndDate()
        {
            lock (lockObject)
            {
                _endWorkingDate = DateTime.Now;
                _workingDuration = _endWorkingDate - _startWorkingDate;
            }
        }

        public ExtendedBackgroundWorker(String workerName) : this(workerName, false, -1)
        {

        }

        private void ElapsedEvent(Object sender)
        {
            CalculateWorkingEndDate();
            if (_checkTimeout)
                _timerTimeout.Change(Timeout.Infinite, Timeout.Infinite);
            //this.Abort();
            // Raise event added event
            this.OnWorkerTimeout(new WorkerTimeOutEventArgs(sender));
        }

        private void OnWorkerTimeout(WorkerTimeOutEventArgs args)
        {
            WorkerTimeOutEvent?.Invoke(this);
        }

        private String _workerName;
        public String WorkerName
        {
            get { return _workerName; }
        }

        private Thread mThread;
        private Thread BackgroundThread
        {
            get { return mThread; }
        }


        private bool HasBackgroundThread
        {
            get { return (mThread != null) & this.IsBusy; }
        }

        public TimeSpan WorkingDuration
        {
            get
            {
                return _workingDuration;
            }

            set
            {
                _workingDuration = value;
            }
        }

        /// <summary>
        /// returns the private fields value of the base class using reflection.
        /// </summary>
        /// <typeparam name="T">The expected type of the field to be retrieved</typeparam>
        /// <param name="privateFieldName">The name of the field to look for</param>
        /// <returns>The value of the private field requested in the base class of the caller</returns>
        /// <remarks></remarks>
        private T GetBaseFieldValue<T>(string privateFieldName)
        {

            object objValue = this.GetType().BaseType.GetField(privateFieldName, BindingFlags.Instance | BindingFlags.NonPublic).GetValue(this);


            return SafeCast<T>(objValue);
        }


        /// <summary>
        /// Works like the 'as' operator in C#. If the cast is not successfull
        /// no exception is thrown, and the default value for the requested type is returned.
        /// </summary>
        /// <typeparam name="T">The Type to cast the object into</typeparam>
        /// <param name="value">The object to be casted</param>
        /// <returns>Either the object casted into the requested type or the default value for the type (null, zero etc..)</returns>
        /// <remarks></remarks>
        public T SafeCast<T>(object value)
        {
            if (value == null)
                return default(T);
            if ((value) is T)
                return (T)value;
            return default(T);
        }




        /// <summary>
        /// This method kills the running task.
        /// If you have any exception handling in your 'DoWork' method, 
        /// be sure to also catch and ignore ThreadAbortException which will be raised when this method is called.
        /// If you don't have exception handling inside the DoWork method, you do not need to do anything. The exception will be caught by the component.
        /// </summary>
        /// <remarks></remarks>
        public void Abort()
        {
            if (this.HasBackgroundThread)
            {
                try
                {
                    BackgroundThread.Abort();
                }
                catch (Exception)
                {
                }
                RunWorkerCompletedEventArgs completedArgs = new RunWorkerCompletedEventArgs(null, new WorkerAbortException("Thread Aborted!!!!!"), true);
                AsyncOperation op = GetBaseFieldValue<AsyncOperation>("asyncOperation");
                SendOrPostCallback callback = GetBaseFieldValue<SendOrPostCallback>("operationCompleted");

                op.PostOperationCompleted(callback, completedArgs);
            }
        }

        /// <summary>
        /// We override DoWork so we can get a hold of the currently running thread from the threadpool
        /// </summary>
        /// <param name="args"></param>
        /// <remarks></remarks>
        protected override void OnDoWork(DoWorkEventArgs args)
        {
            mThread = Thread.CurrentThread;

            try
            {
                ResetWorkingStartDate();
                if (_checkTimeout)
                    _timerTimeout.Change(_timeout, Timeout.Infinite);
                base.OnDoWork(args);
            }
            catch (ThreadAbortException)
            {
                //don't let the thread excpetion propogate any further.
                Thread.ResetAbort();
            }

        }

        protected override void OnRunWorkerCompleted(RunWorkerCompletedEventArgs args)
        {
            CalculateWorkingEndDate();
            if (_checkTimeout)
                _timerTimeout.Change(Timeout.Infinite, Timeout.Infinite);
            base.OnRunWorkerCompleted(args);
        }
    }

    //=======================================================
    //Service provided by Telerik (www.telerik.com)
    //Conversion powered by NRefactory.
    //Twitter: @telerik
    //Facebook: facebook.com/telerik
    //=======================================================

    public class WorkerTimeOutEventArgs : EventArgs
    {
        private Object _sender;

        public WorkerTimeOutEventArgs(Object sender)
        {
            _sender = sender;
        }
    }

}
